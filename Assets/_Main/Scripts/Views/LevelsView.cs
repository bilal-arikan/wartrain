using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Arikan;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class LevelsView : UIView<LevelsView>
{
    public Button closeButton;
    public Button leftPageButton;
    public GameObject leftPageAnim;
    public Button rightPageButton;
    public GameObject rightPageAnim;
    public HorizontalScrollSnap scrollSnap;
    public GameObject[] pageRoots;

    public Dictionary<Image, Vector2> cloudsAndMovementSpeeds;

    public Button[] levelButtons;
    public override string Group => "main";

    // Start is called before the first frame update
    void Start()
    {
        closeButton.onClick.AddListener(MenuView.Instance.Open);
        for (int i = 0; i < levelButtons.Length; i++)
        {
            var index = i;
            levelButtons[i].onClick.AddListener(() => SelectLevel(index));
        }
    }
    protected override void OnEnable()
    {
        base.OnEnable();
        ParseLevelButtons();
    }

    private void Update()
    {
        foreach (var cmKV in cloudsAndMovementSpeeds)
        {
            // cmKV.Key.ti += new Vector3(cmKV.Value.x * Time.deltaTime, cmKV.Value.y * Time.deltaTime, 0);
        }
    }

    void SelectLevel(int index)
    {
        var info = LevelList.Instance.GetLevelInfo(index);
        LevelDetailView.Instance.Parse(index, info);
        LevelDetailView.Instance.Open();
    }

    [Button]
    void ParseLevelButtons()
    {
        for (int i = 0; i < levelButtons.Length; i++)
        {
            var state = DataController.Instance.GetLevelState(i);
            switch (state)
            {
                case 0:
                    levelButtons[i].targetGraphic.color = Color.black;
                    break;
                case 1:
                    levelButtons[i].targetGraphic.color = Color.gray;
                    break;
                default:
                    levelButtons[i].targetGraphic.color = Color.green;
                    break;
            }
            levelButtons[i].interactable = state > 0;
            levelButtons[i].GetComponentInChildren<Text>().text = (i + 1).ToString();
        }
    }
}
