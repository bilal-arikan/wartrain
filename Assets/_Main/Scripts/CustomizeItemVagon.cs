using UnityEngine;

[CreateAssetMenu(fileName = "CustomizeItemVagon", menuName = "GDC_WarTrain/CustomizeItemVagon", order = 0)]
public class CustomizeItemVagon : CustomizeItem
{
    public VagonVisualBehaviour vagonHeadVisual;
    public VagonVisualBehaviour vagonOtherVisual;
    public override string ctgry => "vagon_visual";
    public override UnityEngine.Object asset => vagonHeadVisual;
}