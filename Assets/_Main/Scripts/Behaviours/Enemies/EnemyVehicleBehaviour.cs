using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class EnemyVehicleBehaviour : EnemyBehaviour
{
    public VehiclePath path;

    private Cart cart;
    private VehicleAIController aiController;

    protected override void Awake()
    {
        base.Awake();
        cart = GetComponent<Cart>();
        aiController = GetComponent<VehicleAIController>();
        if (path)
        {
            aiController.waypoints = path.path.ToList();
        }
        else
        {
            if (cart)
                cart.enabled = false;
            if (aiController)
                aiController.enabled = false;
        }
    }

    protected override void Start()
    {
        base.Start();
        main.onDeath.AddListener(OnDeath);
        isFocused.AddListener((v) =>
        {
            if (v)
            {
                // Set closest point to train
            }
            else
            {
                // aiController.waypoints = path.path.ToList();
            }
        });

    }

    private void OnDeath()
    {
        if (cart)
            cart.enabled = false;
        if (aiController)
            aiController.enabled = false;
    }

    public Vector3? RandomNavmeshLocation(float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            return hit.position;
        }
        return null;
    }
}