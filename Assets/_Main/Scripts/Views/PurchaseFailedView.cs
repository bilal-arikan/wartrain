using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class PurchaseFailedView : UIView<PurchaseFailedView>
{
    [SerializeField] Button closeButton;
    public GameObject notEnoughMoney;

    public override string Group => "";

    void Start()
    {
        closeButton.onClick.AddListener(Close);
    }
}