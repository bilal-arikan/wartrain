using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Arikan;
using BezierSolution;
using Cinemachine;
using Sirenix.OdinInspector;
using ToolBox.Pools;
using UnityEngine;

public class GameController : SingletonBehaviour<GameController>
{

    void Start()
    {
        SRDebug.Init();
        Physics.gravity *= 2;
        if (LevelController.Instance.currentLevelRoot == null)
        {
            LevelController.Instance.LoadLevel(0);
        }
        MenuView.Instance.Open();

        SocialSystem.Instance.Init().Finally(() =>
        {
            SocialSystemFirebase.Instance.Init().Finally(() =>
            {
            });
        });
    }

    [Button]
    public void PlayGame(int level)
    {
        if (level >= GameResources.Instance.levels.Length)
        {
            ToMenu();
            return;
        }
        Pool.DestroyAllPools();
        LevelController.Instance.RemoveLevel();
        LevelController.Instance.LoadLevel(level);

        LevelController.Instance.RemoveTrain(PlayerController.Instance.currentTrain);
        var trInfo = PlayerController.Instance.GetOurTrainInfo();
        var tr = LevelController.Instance.SpawnTrain(trInfo);
        PlayerController.Instance.SetOurTrain(tr);
        PlayerController.Instance.CameraFollow(0);

        InputNew.SetEnableTrainInputs(true);
        GameView.Instance.Open();
        DataController.LastPlayedLevel.Value = level;
    }

    public void RestartGame() => PlayGame(DataController.LastPlayedLevel);
    public void NextLevel() => PlayGame(DataController.LastPlayedLevel + 1);
    internal void ToMenu()
    {
        LevelController.Instance.RemoveTrain(PlayerController.Instance.currentTrain);
        InputNew.SetEnableTrainInputs(false);
        MenuView.Instance.Open();
    }
    public void FinishLevel()
    {
        InputNew.SetEnableTrainInputs(false);
        FinishView.Instance.Parse(LevelList.Instance.GetLevelInfo(LevelController.Instance.CurrentLevelIndex).rewardMoney);
        FinishView.Instance.Open();
        DataController.Instance.SetLevelState(LevelController.Instance.CurrentLevelIndex, 2);
        GiveCurrentLevelrewards();

        AdsSystem.Instance.ShowInters();
    }
    public void LostLevel()
    {
        InputNew.SetEnableTrainInputs(false);
        LostView.Instance.Open();

        AdsSystem.Instance.ShowInters();
    }

    void GiveCurrentLevelrewards()
    {
        var level = LevelController.Instance.CurrentLevelIndex;
        var rewards = LevelList.Instance.GetLevelInfo(level).rewardMoney;
        DataController.Money.Value += rewards;
    }
}
