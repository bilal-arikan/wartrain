﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arikan. All Rights Reserved.
// Author: Bilal Arikan
// Time  : 27.11.2018   
//***********************************************************************//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using UnityEngine;

namespace Arikan
{
    /// <summary>
    /// Add() ve Remove() methodları doğrudan PlayerPrefse kaydeder
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    [Serializable]
    public class Dictionary_<TKey, TValue> : List_<KeyValuePair<TKey, TValue>>, IPrefObject, IDictionary<TKey, TValue> //Devre dışı çünkü serilize ederken prefsKeys i almıyor
    {
        public Dictionary_(string prefsuniqekey) : base(prefsuniqekey)
        {
        }

        [Newtonsoft.Json.JsonConstructor]
        public Dictionary_(IEnumerable<KeyValuePair<TKey, TValue>> values) : base(values)
        {
        }

        public TValue this[TKey key]
        {
            get
            {
                for (int i = 0; i < base.Values.Count; i++)
                    if (base.Values[i].Key.Equals(key))
                        return base.Values[i].Value;
                throw new KeyNotFoundException();
            }
            set
            {
                Add(key, value);
            }
        }

        public ICollection<TKey> Keys => base.Values.ConvertAll(kv => kv.Key);

        ICollection<TValue> IDictionary<TKey, TValue>.Values => base.Values.ConvertAll(kv => kv.Value);

        public void Add(TKey key, TValue value)
        {
            for (int i = 0; i < base.Values.Count; i++)
            {
                if (object.Equals(key, base.Values[i].Key))
                {
                    base[i] = new KeyValuePair<TKey, TValue>(key, value);
                    return;
                }
            }
            base.Add(new KeyValuePair<TKey, TValue>(key, value));
        }

        public bool ContainsKey(TKey key)
        {
            for (int i = 0; i < base.Values.Count; i++)
                if (object.Equals(base.Values[i].Key, key))
                    return true;
            return false;
        }

        public bool Remove(TKey key)
        {
            for (int i = 0; i < base.Values.Count; i++)
                if (object.Equals(base.Values[i].Key, key))
                {
                    base.RemoveAt(i);
                    return true;
                }
            return false;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            foreach (var kv in base.Values)
                if (object.Equals(kv.Key, key))
                {
                    value = kv.Value;
                    return true;
                }
            value = default(TValue);
            return false;
        }

    }
}

public static class DictPrefExt
{
    public static void AddToList<T1, T2>(this Arikan.Dictionary_<T1, List<T2>> d, T1 key, T2 value)
    {
        d[key].Add(value);
        d.Add(key, d[key]);
    }
}