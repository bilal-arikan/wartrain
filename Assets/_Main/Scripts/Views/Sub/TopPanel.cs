using Arikan;
using RSG;
using UnityEngine;
using UnityEngine.UI;

public class TopPanel : UIView<TopPanel>
{
    override public string Group => "";

    public Button iapButton;
    public GameObject moneyRoot;
    public Text moneyText;

    protected override void Awake()
    {
        base.Awake();
        iapButton.onClick.AddListener(() =>
        {
            IAPView.Instance.Open();
        });

        moneyText.text = $"{DataController.Money.Value}";
        DataController.Money.OnBeforeChanged += (v1, v2) => moneyText.Counter(v2, v1, 0.5f, 0.1f, setFormat: (v) => $"{v}").RunAsPromise(this);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
    }
}