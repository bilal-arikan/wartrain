using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class LevelDetailView : UIView<LevelDetailView>
{
    public Button closeButton;
    public Button startButton;

    public Text levelName;
    public Text levelRewardMoney;
    public Image medalBronze;
    public Image medalSilver;
    public Image medalGold;

    private KeyValuePair<int, LevelInfo> currentLevel;

    public override string Group => "";

    void Start()
    {
        closeButton.onClick.AddListener(Close);
        startButton.onClick.AddListener(() =>
        {
            GameController.Instance.PlayGame(currentLevel.Key);
            Close();
        });
    }

    public void Parse(int index, LevelInfo info)
    {
        currentLevel = new KeyValuePair<int, LevelInfo>(index, info);
        levelName.text = "Level " + (index + 1);
        levelRewardMoney.text = ": " + info.rewardMoney.ToString();
    }
}
