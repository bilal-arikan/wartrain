﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arikan. All Rights Reserved.
// Author: Bilal Arikan
// Time  : 27.11.2018   
//***********************************************************************//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using UnityEngine;

/// <summary>
/// Generic Type ı doğrudan Jsona çevirip PlayerPrefs kaydeder
/// </summary>
/// <typeparam name="TObj"></typeparam>
[Serializable]
public class PrefObj<TObj> : IPrefObject
{
    /// <summary>
    /// Playerprefs de bu Key değeri ile tutuluyor
    /// </summary>
    [field: SerializeField]
    [Newtonsoft.Json.JsonProperty("K")]
    public string Key { get; protected set; }
    protected TObj value;

    /// <summary>
    /// Constructor a girilen default değer
    /// </summary>
    [JsonIgnore]
    public TObj DefaultValue { get; protected set; }
    /// <summary>
    /// Değerdki değişimleri burdan yakalayabiliriz
    /// </summary>
    [NonSerialized]
    public Action OnOnlyChanged;
    [NonSerialized]
    public Action<TObj> OnChanged;
    /// <summary>
    /// new, old
    /// </summary>
    [NonSerialized]
    public Action<TObj, TObj> OnBeforeChanged;

    private Func<TObj, string> toString;
    private Func<string, TObj> fromString;

    PrefObj()
    {
        // PlayerPrefs.SetString(Uniqekey, JsonConvert.SerializeObject(value));
        OnBeforeChanged?.Invoke(value, value);
        OnChanged?.Invoke(value);
        //Debug.Log("___ PrefObj 1 " + value);
    }

    public PrefObj(string uniqekey) : this(uniqekey, default(TObj)) { }
    public PrefObj(string uniqekey, TObj value) : this(uniqekey, value, (v) => JsonConvert.SerializeObject(v), JsonConvert.DeserializeObject<TObj>) { }
    public PrefObj(string uniqekey, TObj value, Func<TObj, string> toString, Func<string, TObj> fromString)
    {
        this.Key = uniqekey;
        this.DefaultValue = value;
        this.toString = toString;
        this.fromString = fromString;
        //Debug.Log("___ PrefObj 3 " + value);

        if (PlayerPrefs.HasKey(Key))
        {
            //Debug.Log("CREATEED___   " + Uniqekey + value);
            this.value = fromString(PlayerPrefs.GetString(Key));
        }
        else
        {
            //Debug.Log("CREATEED222222222222___   " + Uniqekey);
            this.value = value;
            // PlayerPrefs.SetString(Uniqekey, JsonConvert.SerializeObject(value));
        }
        // OnBeforeChanged?.Invoke(value, value);
        // OnChanged?.Invoke(value);
    }

    [Newtonsoft.Json.JsonProperty("V")]
    [ShowInInspector]
    public virtual TObj Value
    {
        get { return value; }
        set
        {
            //Debug.Log(this.value + "_SETTDIRECT__" + value);
            TObj temp = this.value;
            this.value = value;
            OnBeforeChanged?.Invoke(this.value, temp);
            PlayerPrefs.SetString(Key, toString(value));
            OnChanged?.Invoke(value);
        }
    }

    public bool HasValue()
    {
        return PlayerPrefs.HasKey(Key);
    }
    public object GetValue()
    {
        return value;
    }
    public object GetDefaultValue()
    {
        return DefaultValue;
    }
    public void Save()
    {
        PlayerPrefs.SetString(Key, toString(value));
        PlayerPrefs.Save();
    }

    public static implicit operator TObj(PrefObj<TObj> obj) => obj.Value; //obj == null ? default(TObj) : obj.Value

    public static bool operator ==(PrefObj<TObj> obj2, TObj obj1) => obj1.Equals(obj2.value);
    public static bool operator !=(PrefObj<TObj> obj2, TObj obj1) => !obj1.Equals(obj2.value);
    public static bool operator ==(TObj obj1, PrefObj<TObj> obj2) => obj1.Equals(obj2.value);
    public static bool operator !=(TObj obj1, PrefObj<TObj> obj2) => !obj1.Equals(obj2.value);

    // public void Reset() => Value = DefValue.CreateCopy();
    public override bool Equals(object obj) => value.Equals(obj);
    public override int GetHashCode() => value.GetHashCode();
    public override string ToString() => this.toString(value);
}

[JsonConverter(typeof(PrefObjConverter<bool>))]
public class bool_ : PrefObj<bool> { public bool_(string uniqekey, bool value = false) : base(uniqekey, value) { } }
[JsonConverter(typeof(PrefObjConverter<int>))]
public class int_ : PrefObj<int>
{
    public int multiplier = 1;
    [Newtonsoft.Json.JsonProperty("V")]
    [SerializeField]
    public override int Value
    {
        get { return value; }
        set
        {
            int temp = this.value;
            this.value = this.value - value < 0 ? this.value + (multiplier * Mathf.Abs(this.value - value)) : value;
            OnBeforeChanged?.Invoke(this.value, temp);
            // PlayerPrefs.SetString(Uniqekey, JsonConvert.SerializeObject(value));
            PlayerPrefs.SetString(Key, value.ToString());
            OnChanged?.Invoke(value);
        }
    }
    public int_(string uniqekey, int value = 0, int multiplier = 1) : base(uniqekey, value)
    {
        this.multiplier = multiplier;
    }
}
[JsonConverter(typeof(PrefObjConverter<long>))]
public class long_ : PrefObj<long> { public long_(string uniqekey, long value = 0) : base(uniqekey, value, (v) => v.ToString(), long.Parse) { } }
[JsonConverter(typeof(PrefObjConverter<string>))]
public class string_ : PrefObj<string> { public string_(string uniqekey, string value = null) : base(uniqekey, value, (v) => v, (v) => value) { } }
[JsonConverter(typeof(PrefObjConverter<float>))]
public class float_ : PrefObj<float> { public float_(string uniqekey, float value = 0) : base(uniqekey, value, (v) => v.ToString(), float.Parse) { } }
[JsonConverter(typeof(PrefObjConverter<double>))]
public class double_ : PrefObj<double> { public double_(string uniqekey, double value = 0) : base(uniqekey, value, (v) => v.ToString(), double.Parse) { } }
[JsonConverter(typeof(PrefObjConverter<DateTime>))]
public class DateTime_ : PrefObj<DateTime> { public DateTime_(string uniqekey, DateTime value) : base(uniqekey, value) { } }
[JsonConverter(typeof(PrefObjConverter<Color>))]
public class Color_ : PrefObj<Color> { public Color_(string uniqekey, Color value) : base(uniqekey, value) { } }
[JsonConverter(typeof(PrefObjConverter<TimeSpan>))]
public class TimeSpan_ : PrefObj<TimeSpan> { public TimeSpan_(string uniqekey, TimeSpan value) : base(uniqekey, value) { } }


public class PrefObjConverter<T> : JsonConverter<PrefObj<T>>
{
    public override PrefObj<T> ReadJson(JsonReader reader, Type objectType, PrefObj<T> existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
        try
        {
            // Debug.Log("ReadJson " + existingValue?.GetType().FullName);
            existingValue.Value = Newtonsoft.Json.Linq.JToken.Load(reader).ToObject<T>();
        }
        catch (System.Exception e)
        {
            Debug.LogException(e);
        }
        return existingValue;
    }
    public override void WriteJson(JsonWriter writer, PrefObj<T> value, JsonSerializer serializer)
    {
        // Debug.Log("WriteJson " + value?.GetType().FullName);
        writer.WriteValue(value.Value);
    }
}

public interface IPrefObject
{
    string Key { get; }
    object GetValue();
    object GetDefaultValue();
    void Save();
}

// public static class PrefObjectExts
// {
//     public static object GetValue(this IPrefObject obj)
//     {
//         return obj.GetType().GetProperty("Value").GetValue(obj);
//     }
//     public static object GetDefaultValue(this IPrefObject obj)
//     {
//         return obj.GetType().GetProperty("DefaultValue").GetValue(obj);
//     }