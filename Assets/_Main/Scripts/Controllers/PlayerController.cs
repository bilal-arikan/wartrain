using System.Linq;
using Arikan;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : SingletonBehaviour<PlayerController>
{
    [ShowInInspector, ReadOnly] public TrainBehaviour currentTrain { get; private set; }
    [Header("Aim")]
    public AimPoint aimIndicator;
    public GameObject camReticle;
    [Header("Ray")]
    public LayerMask raycastTargetMask;
    public float rayCastDistance = 1000f;
    [ShowInInspector, ReadOnly] public int RaycastHitCount { get; private set; }
    [ShowInInspector, ReadOnly] public RaycastHit[] RaycastHits { get; private set; }
    [ShowInInspector, ReadOnly] public Vector3? cameraFocusedPoint { get; private set; }
    [ShowInInspector, ReadOnly] public Collider cameraFocusedCollider { get; private set; }
    private Vector3? cameraFocusedPointOld = null;
    [ShowInInspector, ReadOnly] public bool showAim { get; private set; }


    private Ray camerOriginRay;
    private bool oldShowAim;

    protected override void Awake()
    {
        RaycastHits = new RaycastHit[GameResources.Instance.aimRaycastMaxHitAmount];
        base.Awake();
        foreach (var item in CustomizeResources.Instance.initiallyOwnedItems)
        {
            CustomizeController.Instance.GiveItem(item.id);
        }
    }

    private void Update()
    {
        if (currentTrain != null && CameraController.Instance.currentMode != CameraMode.Orbit)
        {
            var camTransform = Camera.main.transform;
            camerOriginRay = new Ray(camTransform.position, camTransform.forward);
            RaycastHitCount = Physics.RaycastNonAlloc(camerOriginRay, RaycastHits, rayCastDistance, raycastTargetMask);
            if (RaycastHitCount > 0)
            {
                for (int i = 0; i < RaycastHitCount; i++)
                {
                    if (RaycastHits[RaycastHitCount - 1 - i].rigidbody != null)
                    {
                        cameraFocusedPoint = RaycastHits[RaycastHitCount - 1 - i].point;
                        cameraFocusedCollider = RaycastHits[RaycastHitCount - 1 - i].collider;
                        goto forBreak;
                    }
                }
                cameraFocusedPoint = RaycastHits[RaycastHitCount - 1].point;
                cameraFocusedCollider = RaycastHits[RaycastHitCount - 1].collider;
            forBreak:
                ;
            }
            else
            {
                cameraFocusedPoint = camerOriginRay.GetPoint(rayCastDistance);
                cameraFocusedCollider = null;
            }
        }
        else
        {
            RaycastHitCount = 0;
            cameraFocusedPoint = null;
        }
        cameraFocusedPointOld = cameraFocusedPoint;

        showAim = currentTrain?.weaponsActivatedVagon != null && currentTrain.weaponsActivatedVagon.weaponsFocus.Any(f => f);
        if (showAim != oldShowAim)
        {
            aimIndicator.gameObject.SetActive(showAim);
            camReticle.gameObject.SetActive(showAim);
        }
        oldShowAim = showAim;
        if (showAim && cameraFocusedPoint.HasValue)
            aimIndicator.SetAimPoint(RaycastHits[0]);
        // aimIndicator.tr.position = cameraFocusedPoint.Value;
    }

    public VagonBehaviour[] GetOurTrainInfo()
    {
        var vagons = new VagonBehaviour[]{
            GameResources.Instance.trainHeadPrefab,
            GameResources.Instance.trainVagonPrefab,
            GameResources.Instance.trainVagonPrefab
        };
        return vagons;
    }

    public void SetOurTrain(TrainBehaviour train)
    {
        currentTrain = train;
        if (train != null)
        {
            // Init Camera
            CameraFollow(currentTrain);

            // Init Weapons
            var items = CustomizeController.Instance.GetAppliedItems();
            foreach (var item in items)
            {
                CustomizeController.Instance.ApplyItem(item.id, null);
            }
        }
    }

    void FillWeapons(VagonBehaviour vagon, WeaponBehaviour weapPrefab)
    {
        vagon.GetComponentsInChildren<WeaponBehaviour>(true).ForEach(w => Destroy(w.gameObject));
        vagon.weapons = new WeaponBehaviour[0];
        if (weapPrefab == null)
            return;
        if (weapPrefab.weaponAmount == WeaponAmount.One)
        {
            var v = Instantiate(weapPrefab, vagon.mainWeaponPoint.position, vagon.mainWeaponPoint.rotation, vagon.transform);
            vagon.weapons = new[] { v };
        }
        else if (weapPrefab.weaponAmount == WeaponAmount.Two)
        {
            WeaponBehaviour[] v = new WeaponBehaviour[2];
            v[0] = Instantiate(weapPrefab, vagon.subWeaponPoints[0].position, vagon.subWeaponPoints[0].rotation, vagon.transform);
            v[1] = Instantiate(weapPrefab, vagon.subWeaponPoints[1].position, vagon.subWeaponPoints[1].rotation, vagon.transform);
            vagon.weapons = v;
        }
        else
        {
            WeaponBehaviour[] v = new WeaponBehaviour[vagon.subWeaponPoints.Length];
            for (int i = 0; i < vagon.subWeaponPoints.Length; i++)
            {
                v[i] = Instantiate(weapPrefab, vagon.subWeaponPoints[i].position, vagon.subWeaponPoints[i].rotation, vagon.transform);
            }
            vagon.weapons = v;
        }
    }
    void FillVisuals(VagonVisualBehaviour head, VagonVisualBehaviour other)
    {
        foreach (var vagon in currentTrain.vagonsWithHead)
        {
            if (vagon.visualObject != null)
                Destroy(vagon.visualObject.gameObject);

            if (vagon is TrainBehaviour)
                vagon.visualObject = Instantiate(head, vagon.transform);
            else
                vagon.visualObject = Instantiate(other, vagon.transform);
        }
    }

    public void CameraFollow(int vagonIndex) => CameraFollow(currentTrain.vagonsWithHead.ElementAt(vagonIndex));
    public void CameraFollow(VagonBehaviour vagon)
    {
        CameraController.Instance.SetTarget(vagon.mainWeaponPoint);
        CameraController.Instance.ChangeCameraMode(vagon.weapons.FirstOrDefault()?.GetCamMode() ?? CameraMode.Orbit);
        foreach (var v in currentTrain.vagonsWithHead)
        {
            if (vagon == v)
                currentTrain.weaponsActivatedVagon = v;
        }
        GameView.Instance.UpdateUI();
    }

    // For AutoFire
    [Button]
    public bool AutoFireActivated(out TargetBehaviour closest, out Vector3 aimPoint)
    {
        var activateFire = false;
        aimPoint = Vector3.zero;
        closest = null;
        for (int i = 0; i < RaycastHitCount; i++)
        {
            var hitTag = RaycastHits[i].collider.attachedRigidbody?.tag;
            if (hitTag == "Player")
            {
                return false;
            }
            else if (hitTag == "Shootable")
            {
                aimPoint = RaycastHits[i].point;
                closest = RaycastHits[i].collider.attachedRigidbody?.GetComponent<TargetBehaviour>();
                activateFire = activateFire || !closest.IsDeath;
            }
            else if (!activateFire && RaycastHits[i].collider.tag == "FireArea")
            {
                aimPoint = RaycastHits[i].point;
                closest = RaycastHits[i].collider.attachedRigidbody?.GetComponent<TargetBehaviour>();
                activateFire = !closest.IsDeath;
            }
        }
        return activateFire;
    }

    public void ApplyItem(CustomizeItemWeapon ciWeapon)
    {
        if (currentTrain == null)
            return;
        var wp = ciWeapon.weaponBehaviour;
        switch (wp.weaponCtgry)
        {
            case WeaponCtgry.AntiGround:
                FillWeapons(currentTrain.vagonsWithHead.ElementAt(0), wp);
                break;
            case WeaponCtgry.Hovitzer:
                FillWeapons(currentTrain.vagonsWithHead.ElementAt(1), wp);
                break;
            case WeaponCtgry.AntiAir:
                FillWeapons(currentTrain.vagonsWithHead.ElementAt(2), wp);
                break;
            case WeaponCtgry.MissileGround:
                FillWeapons(currentTrain.vagonsWithHead.ElementAt(3), wp);
                break;
            case WeaponCtgry.MissileAir:
                FillWeapons(currentTrain.vagonsWithHead.ElementAt(4), wp);
                break;
        }
    }
    public void ApplyItem(CustomizeItemVagon ciVagon)
    {
        if (currentTrain == null)
            return;
        FillVisuals(ciVagon.vagonHeadVisual, ciVagon.vagonOtherVisual);
    }

    private void OnDrawGizmosSelected()
    {
        if (RaycastHitCount > 0)
        {
            Gizmos.color = Color.red;
            for (int i = 0; i < RaycastHits.Length; i++)
            {
                if (RaycastHits[i].collider != null)
                {
                    Gizmos.DrawSphere(RaycastHits[i].point, 0.1f);
                }
            }
        }
    }
}