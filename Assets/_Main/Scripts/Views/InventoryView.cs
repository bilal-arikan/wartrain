using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class InventoryView : UIView<InventoryView>
{
    public Button closeButton;

    public MarketItem itemPrefab;
    public Dictionary<string, LayoutGroup> itemsLayoutGroup;

    public override string Group => "main";


    protected override void OnEnable()
    {

        base.OnEnable();
    }
    protected override void OnDisable()
    {
        base.OnDisable();
    }
}
