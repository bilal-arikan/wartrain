using Arikan;
using UnityEngine;

[ResourcePath("Data/GameResources")]
[CreateAssetMenu(fileName = "GameResources", menuName = "GDC_WarTrain/GameResources", order = 0)]
public class GameResources : SingletonScriptableObject<GameResources>
{
    public float trainSpawnNormalizedT = 0.05f;
    public float levelFinishNormalizedT = 0.95f;
    public int aimRaycastMaxHitAmount = 6;
    public LayerMask bulletAreaHitMask;
    public LayerMask bulletDirectHitMask;

    public VagonBehaviour trainHeadPrefab;
    public VagonBehaviour trainVagonPrefab;
    [Space]
    public GameObject[] vagonHeadVisualPrefabs;
    public GameObject[] vagonVisualPrefabs;
    [Space]
    public WeaponBehaviour[] weaponPrefabs;
    [Space]
    public BulletBehaviour[] bulletPrefabs;
    [Space]
    public EnemyBehaviour[] enemyPrefabs;
    [Space]
    public LevelRoot[] levels;
    public ArrowIndicator enemyIndicatorPrefab;
    public ArrowIndicator gateIndicatorPrefab;
    public GateWayBehaviour finishGatePrefab;
    public MeshRenderer[] mountainPrefabs;
}