using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if ZENJECT
using UnityEngine.SceneManagement;
using Zenject;
#endif

namespace ToolBox.Pools
{
    public sealed class Pool
    {
        private readonly GameObject _originalPrefab = null;
        private readonly Poolable _prefabCopy = null;
        private readonly Stack<Poolable> _pooledInstances = null;
        private readonly Quaternion _rotation = default;
        private readonly Vector3 _scale = default;
        private bool markedForDestruction = false;
#if ZENJECT
        private readonly DiContainer _projectContainer = null;
        private DiContainer _sceneContainer = null;
        private Scene _currentScene = default;
#endif

        private static readonly Dictionary<GameObject, Pool> _prefabLookup = new Dictionary<GameObject, Pool>(64);
        private static readonly Dictionary<GameObject, Pool> _instanceLookup = new Dictionary<GameObject, Pool>(512);

        private const int CAPACITY = 128;

        internal Pool(GameObject prefab)
        {
            if (_prefabLookup.TryGetValue(prefab, out var pool))
            {
                throw new System.Exception($"Prefab {prefab.name} is already in use by another pool");
            }
            else
            {
                _originalPrefab = prefab;
                _prefabCopy = Object.Instantiate(prefab).AddComponent<Poolable>();
                Object.DontDestroyOnLoad(_prefabCopy);
                _prefabCopy.gameObject.SetActive(false);
                _prefabLookup.Add(prefab, this);
            }

#if ZENJECT
            _projectContainer = ProjectContext.Instance.Container;
            UpdateContainer();
#endif
            _pooledInstances = new Stack<Poolable>(CAPACITY);

            var transform = prefab.transform;
            _rotation = transform.rotation;
            _scale = transform.localScale;
        }
        ~Pool()
        {
            // Object.Destroy(_prefabCopy.gameObject);
            Debug.Log("~Pool " + _originalPrefab.name);
        }

        public static Pool GetPrefabPool(GameObject prefab)
        {
            bool hasPool = _prefabLookup.TryGetValue(prefab, out var pool);

            if (!hasPool)
            {
                pool = new Pool(prefab);
            }

            return pool;
        }

        public static void DestroyPool(GameObject prefab)
        {
            if (_prefabLookup.TryGetValue(prefab, out var pool))
            {
                pool.UnPopulate(pool._pooledInstances.Count);

                _prefabLookup.Remove(pool._originalPrefab);
                pool.markedForDestruction = true;
            }
        }

        public static void DestroyAllPools()
        {
            foreach (var poolObj in _prefabLookup.Keys.ToList())
            {
                DestroyPool(poolObj);
            }
        }

        public static bool TryGetInstancePool(GameObject instance, out Pool pool) =>
            _instanceLookup.TryGetValue(instance, out pool);

        public void Populate(int count)
        {
            markedForDestruction = false;
            for (int i = 0; i < count; i++)
                _pooledInstances.Push(CreateInstance());
        }

        public void UnPopulate(int count)
        {
            // Debug.Log($"Stack {count}:{_pooledInstances.Count}:{_instanceLookup.Count}");

            count = Mathf.Min(count, _pooledInstances.Count);
            for (int i = 0; i < count; i++)
            {
                var go = _pooledInstances.Pop().gameObject;
                _instanceLookup.Remove(go);
                Object.Destroy(go);
            }
        }
        public void DestroyActives()
        {

        }

        public GameObject Get()
        {
            var instance = GetInstance();

            return instance.gameObject;
        }

        public GameObject Get(Transform parent)
        {
            var instance = GetInstance();

            instance.transform.SetParent(parent);

            return instance.gameObject;
        }

        public GameObject Get(Transform parent, bool worldPositionStays)
        {
            var instance = GetInstance();

            instance.transform.SetParent(parent, worldPositionStays);

            return instance.gameObject;
        }

        public GameObject Get(Vector3 position, Quaternion rotation)
        {
            var instance = GetInstance();

            instance.transform.SetPositionAndRotation(position, rotation);

            return instance.gameObject;
        }

        public GameObject Get(Vector3 position, Quaternion rotation, Transform parent)
        {
            var instance = GetInstance();
            var instanceTransform = instance.transform;

            instanceTransform.SetPositionAndRotation(position, rotation);
            instanceTransform.SetParent(parent);

            return instance.gameObject;
        }

        public void Release(GameObject instance)
        {
            var poolable = instance.GetComponent<Poolable>();
            poolable.OnRelease();

            instance.SetActive(false);

            if (markedForDestruction)
            {
                _instanceLookup.Remove(instance);
                Object.Destroy(instance);
            }
            else
            {
                var instanceTransform = instance.transform;
                instanceTransform.SetParent(null);
                instanceTransform.rotation = _rotation;
                instanceTransform.localScale = _scale;

                _pooledInstances.Push(poolable);
            }
        }

        private Poolable GetInstance()
        {
            int count = _pooledInstances.Count;

            if (count != 0)
            {
                var instance = _pooledInstances.Pop();

                if (instance == null)
                {
                    count--;

                    while (count != 0)
                    {
                        instance = _pooledInstances.Pop();

                        if (instance != null)
                        {
                            instance.OnGet();
                            instance.gameObject.SetActive(true);

                            return instance;
                        }

                        count--;
                    }

                    instance = CreateInstance();
                    instance.gameObject.SetActive(true);

                    return instance;
                }
                else
                {
                    instance.OnGet();
                    instance.gameObject.SetActive(true);

                    return instance;
                }
            }
            else
            {
                var instance = CreateInstance();
                instance.gameObject.SetActive(true);

                return instance;
            }
        }

        private Poolable CreateInstance()
        {
            var instance = Object.Instantiate(_prefabCopy);
            var instanceGameObject = instance.gameObject;
            _instanceLookup.Add(instanceGameObject, this);
#if ZENJECT
            if (!_currentScene.isLoaded)
                UpdateContainer();
            
            _sceneContainer.InjectGameObject(instanceGameObject);
#endif

            return instance;
        }

#if ZENJECT
        private void UpdateContainer()
        {
            _currentScene = SceneManager.GetActiveScene();
            _sceneContainer = _projectContainer.Resolve<SceneContextRegistry>()
                .GetContainerForScene(_currentScene);
        }
#endif
    }
}