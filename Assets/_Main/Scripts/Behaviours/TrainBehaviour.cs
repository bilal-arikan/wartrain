using System.Collections.Generic;
using System.Linq;
using BezierSolution;
using UnityEngine;

public class TrainBehaviour : VagonBehaviour
{
    [Header("Train Head")]
    public List<VagonBehaviour> vagons = new List<VagonBehaviour>();
    public IEnumerable<VagonBehaviour> vagonsWithHead => vagons.Prepend(this);
    public BezierWalkerWithSpeed walker;
    // public BezierWalkerLocomotion locomotion;
    public BezierWalkerLocomotionCustom locomotion;
    [Space]
    public float accelerationLerp = 1;
    public float breakLerp = 5;
    public float maxSpeed = 20;

    public float currentSpeed { get; private set; }
    public bool isBreaking { get; private set; }
    public VagonBehaviour weaponsActivatedVagon { get; set; }

    protected override void Awake()
    {
        base.Awake();
        head = this;
        locomotion.highQuality = false;
        main.onDeath.AddListener(() =>
        {
            GameController.Instance.LostLevel();
        });
        main.onDamaged.AddListener(() =>
        {
            GameView.Instance.ShowDamaged();
            GameView.Instance.SetVagonHealth(
                PlayerController.Instance.currentTrain.main.health,
                PlayerController.Instance.currentTrain.main.maxHealth);
        });
    }
    protected override void Start()
    {
        base.Start();
        GameView.Instance.SetVagonHealth(
            PlayerController.Instance.currentTrain.main.health,
            PlayerController.Instance.currentTrain.main.maxHealth);
    }

    protected override void LateUpdate()
    {
        base.LateUpdate();
        // acceleratorInput = Mathf.Clamp01(Input.GetAxis("Vertical"));
        var targetSpeed = Mathf.Lerp(0, maxSpeed, InputNew.AccelerateInput);
        isBreaking = InputNew.BreakInput > 0 || !InputNew.IsEnabledTrainInputs();

        if (isBreaking || LevelController.Instance.isBreakNecessary)
        {
            currentSpeed = Mathf.Clamp(Mathf.Lerp(currentSpeed, 0, breakLerp * Time.deltaTime), 0.0001f, maxSpeed);
        }
        else
        {
            currentSpeed = Mathf.Clamp(Mathf.Lerp(currentSpeed, targetSpeed, accelerationLerp * Time.deltaTime), 0.0001f, maxSpeed);
        }

        walker.speed = currentSpeed;
    }

    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        var currentGate = other.GetComponentInParent<GateWayBehaviour>();
        if (currentGate != null)
        {
            currentGate.ActivateBy(this);
        }
    }
    protected override void OnTriggerExit(Collider other)
    {
        base.OnTriggerExit(other);
    }

    public void LinkVagon(VagonBehaviour vagon, int vagonIndex)
    {
        vagon.head = this;
        locomotion.AddToTail(vagon.transform, 17f);
        vagons.Add(vagon);

        vagon.main.onDeath.AddListener(() =>
        {
            GameController.Instance.LostLevel();
        });
    }
}