using Arikan;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputController : SingletonBehaviour<InputController>
{
    // [ShowInInspector] public bool isEnabledTrainInputs = InputNew.IsEnabledTrainInputs();

    private void Start()
    {
        InputNew.GetControl();
    }
}

public static class InputNew
{

    private static @NewControls controls;

    private static InputAction accelerateInput;
    private static InputAction breakInput;
    private static InputAction fireInput;
    private static InputAction lookActivateInput;
    private static InputAction lookInput;

    public static float AccelerateInput => accelerateInput.ReadValue<float>();
    public static float BreakInput => breakInput.ReadValue<float>();
    public static float FireInput => fireInput.ReadValue<float>();
    public static float LookActivateInput => lookActivateInput.ReadValue<float>();
    public static Vector2 LookInput => lookInput.ReadValue<Vector2>();

    public static @NewControls GetControl()
    {
        if (controls == null)
        {
            controls = new @NewControls();
        }
        accelerateInput = controls.Train.Accelerate;
        breakInput = controls.Train.Break;
        fireInput = controls.Train.Fire;
        lookActivateInput = controls.Train.LookActivate;
        lookInput = controls.Train.Look;

        return controls;
    }

    public static bool IsEnabledTrainInputs() => GetControl().Train.enabled;
    public static void SetEnableTrainInputs(bool enabled)
    {
        if (enabled)
            GetControl().Train.Enable();
        else
            GetControl().Train.Disable();
    }

}