using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using ToolBox.Pools;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public enum WeaponCtgry
{
    AntiGround,
    AntiAir,
    Hovitzer,
    MissileGround,
    MissileAir,
}

public enum WeaponAmount
{
    One,
    Two,
    Full,
}

public class WeaponBehaviour : MonoBehaviour
{
    public WeaponCtgry weaponCtgry = WeaponCtgry.AntiGround;
    public WeaponAmount weaponAmount = WeaponAmount.One;
    public float fireRateDelay = 0.5f;
    public float rotationLerp = 2f;
    public float accuracy = 0.9f;
    public float focusedAngle = 1.2f;
    public float bulletSpeedMultiply = 1f;

    [MinMaxSlider(-180f, 180f)]
    public Vector2 rotationHorizontalClamp;
    [MinMaxSlider(-90, 90)]
    public Vector2 rotationVerticalClamp;
    [Space]
    public Transform bulletSpawnTransform;
    public Transform directionHorizontalAim;
    public Transform directionVerticalAim;
    public Transform recoilTransform;
    public float recoilDistance = 0.1f;
    public float recoilDuration = 0.1f;
    public BulletBehaviour bulletPrefab;
    public MeshRenderer angelIndicator;
    public LineRenderer trajectoryLine;
    public ParticleSystem[] muzzleEffect;

    public UnityEvent onFocused;
    public UnityEvent onUnFocused;

    public bool IsFocused { get; private set; }

    private bool isFocusedOld;
    private float lastShootTime;
    private Quaternion defaultHorizontalRotation;
    private float trajectoryVerticalAngle;

    private void Awake()
    {
        foreach (var particle in bulletPrefab.explosionParticle.GetComponentsInChildren<ParticleSystem>())
        {
            var main = particle.main;
            main.playOnAwake = false;
            main.stopAction = ParticleSystemStopAction.None;
            main.simulationSpace = ParticleSystemSimulationSpace.World;
        }
        for (int i = 0; i < muzzleEffect.Length; i++)
        {
            var subS = muzzleEffect[i].GetComponentsInChildren<ParticleSystem>();
            for (int j = 0; j < subS.Length; j++)
            {
                var mainS = subS[j].main;
                mainS.playOnAwake = false;
                mainS.loop = false;
                mainS.simulationSpace = ParticleSystemSimulationSpace.World;
                mainS.stopAction = ParticleSystemStopAction.None;
                subS[j].Stop();
            }
        }
    }

    void Start()
    {
        defaultHorizontalRotation = directionHorizontalAim.localRotation;
        // angelIndicator.material.SetFloat("_Arc1", 180 + rotationHorizontalClamp.x);
        // angelIndicator.material.SetFloat("_Arc2", 180 - rotationHorizontalClamp.y);
        angelIndicator.material.SetFloat("_Angle", (rotationHorizontalClamp.x / 180));
        onUnFocused.Invoke();
    }
    private void OnEnable()
    {
        // if (Pool.GetPrefabPool(bulletPrefab.gameObject) == null)
        {
            bulletPrefab.gameObject.Populate((int)(bulletPrefab.autoDestroyDelay / fireRateDelay) + 2);
            bulletPrefab.explosionParticle.gameObject.Populate((int)(bulletPrefab.autoDestroyDelay / fireRateDelay) + 2);
        }
    }
    private void OnDisable()
    {
        if (!Application.isPlaying)
            return;
        bulletPrefab.gameObject.UnPopulate((int)(bulletPrefab.autoDestroyDelay / fireRateDelay) + 2);
        bulletPrefab.explosionParticle.gameObject.UnPopulate((int)(bulletPrefab.autoDestroyDelay / fireRateDelay) + 2);

        IsFocused = false;
        trajectoryLine.gameObject.SetActive(false);
        onUnFocused.Invoke();
    }

    void LateUpdate()
    {
        if (IsFocused != isFocusedOld)
        {
            if (IsFocused)
                onFocused.Invoke();
            else
                onUnFocused.Invoke();
            trajectoryLine.gameObject.SetActive(IsFocused);
            isFocusedOld = IsFocused;
        }
    }

    public bool LookAtUpdateTarget(Vector3 target, out float angleDifference, bool trajectoryShoot = true, bool drawTrajectory = true)
    {
        angleDifference = 0;
        if (enabled == false)
            return false;
        bool canHit = true;

        var lockOnRot = (Quaternion.Inverse(directionHorizontalAim.parent.rotation) * Quaternion.LookRotation(target - directionHorizontalAim.position)).eulerAngles;
        float targetY = lockOnRot.y;
        lockOnRot = lockOnRot.ClampAngleY(rotationHorizontalClamp.x, rotationHorizontalClamp.y);

        canHit = targetY == lockOnRot.y;

        // can trajectory shoot hit the target?
        if (canHit && trajectoryShoot)
        {
            canHit = Ballistics.CalculateTrajectory(
                    trajectoryLine.transform.position,
                    PlayerController.Instance.cameraFocusedPoint.Value,
                    bulletPrefab.bulletSpeed,
                    Physics.gravity,
                    out trajectoryVerticalAngle);
            canHit = canHit && trajectoryVerticalAngle < rotationHorizontalClamp.y;
        }

        if (canHit)
        {
            directionHorizontalAim.localRotation = Quaternion.Slerp(
                directionHorizontalAim.localRotation,
                Quaternion.Euler(0, lockOnRot.y, 0), // trajectory difference
                Time.deltaTime * rotationLerp);

            directionVerticalAim.localRotation = Quaternion.Slerp(
                directionVerticalAim.localRotation,
                trajectoryShoot ? Quaternion.Euler(trajectoryVerticalAngle + lockOnRot.x, 0, 0) : Quaternion.identity,
                Time.deltaTime * rotationLerp);

            if (drawTrajectory)
            {
                // // Get Trajectory points
                // var path = Ballistics.GetBallisticPath(
                //     trajectoryLine.transform.position,
                //     directionVerticalAim.forward,
                //     bulletPrefab.bulletSpeed,
                //     Physics.gravity,
                //     .1f,
                //     3f
                // );
                // trajectoryLine.positionCount = path.Length;
                // trajectoryLine.SetPositions(path);
            }

            angleDifference = Quaternion.Angle(directionVerticalAim.rotation, directionHorizontalAim.parent.rotation * Quaternion.Euler(trajectoryVerticalAngle + lockOnRot.x, targetY, 0));
            // Debug.Log(angleDifference);
            IsFocused = angleDifference < focusedAngle;
            return true;
        }
        else
        {
            LookAtUpdateDefault();
            IsFocused = false;
            return false;
        }
    }
    public bool LookAtUpdateByEnemy(Vector3 target)
    {
        if (enabled == false)
            return false;
        var lockOnRot = (Quaternion.Inverse(directionHorizontalAim.parent.rotation) * Quaternion.LookRotation(target - directionHorizontalAim.position)).eulerAngles;

        directionHorizontalAim.localRotation = Quaternion.Slerp(
            directionHorizontalAim.localRotation,
            Quaternion.Euler(0, lockOnRot.y, 0), // trajectory difference
            Time.deltaTime * rotationLerp);

        directionVerticalAim.localRotation = Quaternion.Slerp(
            directionVerticalAim.localRotation,
             Quaternion.Euler(lockOnRot.x, 0, 0),
            Time.deltaTime * rotationLerp);

        var angle = Quaternion.Angle(directionVerticalAim.rotation, directionHorizontalAim.parent.rotation * Quaternion.Euler(lockOnRot.x, lockOnRot.y, 0));
        // Debug.Log(angle);
        IsFocused = angle < focusedAngle;
        return true;
    }


    public bool LookAtUpdateDefault()
    {
        if (enabled == false)
            return false;
        directionHorizontalAim.localRotation = Quaternion.Slerp(directionHorizontalAim.localRotation, defaultHorizontalRotation, Time.deltaTime * rotationLerp);
        directionVerticalAim.localRotation = Quaternion.Slerp(directionVerticalAim.localRotation, Quaternion.identity, Time.deltaTime * rotationLerp); ;
        IsFocused = false;
        return IsFocused;
    }

    public bool TryFire(bool zeroGravity = false)
    {
        if ((Time.time - lastShootTime) > fireRateDelay)
        {
            Fire(zeroGravity);
            lastShootTime = Time.time;
            return true;
        }
        return false;
    }
    private void Fire(bool zeroGravity)
    {
        var bObj = bulletPrefab.gameObject.Get(bulletSpawnTransform.position, directionVerticalAim.rotation);
        // var bObj = ObjectPooler.Instance.Instantiate(bulletPrefab.name, bulletSpawnTransform.position, directionVerticalAim.rotation);
        Debug.Log("Bullet " + bObj.name + " fired", bObj);
        var bullet = bObj.GetComponent<BulletBehaviour>();
        bullet.MuliplySpeed(bulletSpeedMultiply);
        // var bullet = Instantiate(bulletPrefab, bulletSpawnTransform.position, directionVerticalAim.rotation);
        bullet.body.useGravity = !zeroGravity;
        bullet.TimedDisable();
        for (int i = 0; i < muzzleEffect.Length; i++)
        {
            muzzleEffect[i].Play();
        }
        recoilTransform.DOPunchPosition(Vector3.back * recoilDistance, Mathf.Min(recoilDuration, fireRateDelay), 1, 0);
    }

    public CameraMode GetCamMode()
    {
        switch (weaponCtgry)
        {
            case WeaponCtgry.AntiGround: return CameraMode.Aim;
            case WeaponCtgry.AntiAir: return CameraMode.Fps;
            case WeaponCtgry.Hovitzer: return CameraMode.Mortar;
            case WeaponCtgry.MissileGround: return CameraMode.Aim;
            case WeaponCtgry.MissileAir: return CameraMode.Aim;
        }
        return CameraMode.Orbit;
    }

}
