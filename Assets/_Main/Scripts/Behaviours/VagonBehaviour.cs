using System.Collections.Generic;
using System.Linq;
using BezierSolution;
using Sirenix.OdinInspector;
using UnityEngine;

public class VagonBehaviour : SerializedMonoBehaviour
{
    [ShowInInspector, ReadOnly] public TrainBehaviour head { get; set; }
    public TargetBehaviour main { get; protected set; }
    public VagonVisualBehaviour visualObject;
    public Transform mainWeaponPoint;
    public Transform[] subWeaponPoints;

    public float autoAimDifferenceAmount { get; private set; }
    [ShowInInspector, ReadOnly] public WeaponBehaviour[] weapons = new WeaponBehaviour[0];
    [ShowInInspector, ReadOnly] public bool[] weaponsFocus = new bool[0];

    protected virtual void Awake()
    {
        main = GetComponent<TargetBehaviour>();
    }

    protected virtual void Start()
    {
        main.gameObject.tag = "Player";
    }

    protected virtual void LateUpdate()
    {
        if (weapons.Length != weaponsFocus.Length)
        {
            weaponsFocus = new bool[weapons.Length];
        }
        autoAimDifferenceAmount = 0;

        if (head.weaponsActivatedVagon == this && PlayerController.Instance.cameraFocusedPoint.HasValue)
        {

            // var point = PlayerController.Instance.GetAimedPoint(out bool fire);
            // if (point == null)
            // {
            //     foreach (var wp in weapons)
            //     {
            //         var ready = wp.LookAtUpdateDefault();
            //     }
            //     return;
            // }
            for (int i = 0; i < weapons.Length; i++)
            {
                // var ready = wp.LookAtUpdateTarget(rayHit.Value.point);
                weaponsFocus[i] = weapons[i].LookAtUpdateTarget(PlayerController.Instance.cameraFocusedPoint.Value, out var angleDifference);
                if (weaponsFocus[i])
                {
                    if (InputNew.FireInput > 0)
                    {
                        var fired = weapons[i].TryFire();
                        if (fired)
                        {
                            CameraController.Instance.Shake();
                        }
                    }
                    else if (DataController.AutoFireOn && weapons[i].IsFocused && PlayerController.Instance.AutoFireActivated(out var target, out var point))
                    {
                        var fired = weapons[i].TryFire();
                        if (fired)
                        {
                            CameraController.Instance.Shake();
                        }
                    }
                    autoAimDifferenceAmount += angleDifference / 30;
                }
            }
            if (weapons.Length > 1)
            {
                autoAimDifferenceAmount /= weapons.Length;
            }
        }
        else
        {
            for (int i = 0; i < weapons.Length; i++)
            {
                weaponsFocus[i] = weapons[i].LookAtUpdateDefault();
            }
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {

    }
    protected virtual void OnTriggerExit(Collider other)
    {

    }
}