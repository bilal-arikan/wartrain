using System.Linq;
using Arikan;
using Sirenix.OdinInspector;
using UnityEngine;

public class DataController : Singleton<DataController>
{
    public static int_ Money = new int_("Money", 50);
    public static int_ Gold = new int_("Gold", 20);
    public static int_ Xp = new int_("Xp", 0);
    public static int_ LastPlayedLevel = new int_("LastPlayedLevel", 0);

    public static bool_ Sett_LeftHand = new bool_("Sett_LeftHand", false);
    public static bool_ Sett_Vibration = new bool_("Sett_Vibration", true);
    public static bool_ Sett_Sound = new bool_("Sett_Sound", true);
    public static bool_ Sett_Music = new bool_("Sett_Music", true);
    public static int_ Sett_Graphics = new int_("Sett_Graphics", 0);

    public static bool_ AutoFireOn = new bool_("AutoFireOn", true);

    public static Dictionary_<int, int> LevelStates = new Dictionary_<int, int>("LevelStates");

    public DataController()
    {
        if (!LevelStates.TryGetValue(0, out int state) || state < 1)
        {
            Debug.Log("Level_0 not found. Setting to 1");
            LevelStates[0] = 1;
        }
    }

    [Button]
    public void SetLevelState(int index, int state)
    {
        try
        {
            var totalLevelAmount = GameResources.Instance.levels.Length;
            Debug.Log("SetLevelState: " + index + " " + state + " " + totalLevelAmount);
            for (int i = 0; i < totalLevelAmount; i++)
            {
                if (LevelStates.TryGetValue(i, out int value))
                {
                    LevelStates[i] = Mathf.Max(value, 0);
                }
                else
                {
                    LevelStates[i] = 0;
                }
                if (i == index)
                {
                    LevelStates[i] = state;
                    if (state == 2 && i < (totalLevelAmount - 1))
                    {
                        if (LevelStates.TryGetValue(i + 1, out int value2))
                        {
                            LevelStates[i + 1] = Mathf.Max(value2, 1);
                        }
                        else
                        {
                            LevelStates[i + 1] = 1;
                        }
                    }
                }
                Debug.Log("LevelStates[" + i + "] = " + LevelStates[i]);
            }
        }
        catch (System.Exception e)
        {
            Debug.LogException(e);
        }
    }
    [Button]
    public int GetLevelState(int index)
    {
        return LevelStates.GetValueOrDefault(index, 0);
    }
}