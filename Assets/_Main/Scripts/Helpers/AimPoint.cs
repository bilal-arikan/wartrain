using UnityEngine;

public class AimPoint : MonoBehaviour
{

    public MeshRenderer mortarAreaRend;
    public Transform focusedPoint;

    private Collider oldHitCollider;

    private void Awake()
    {

    }

    public void SetEnableMortar(bool enable, float radius)
    {
        mortarAreaRend.gameObject.SetActive(enable);
        if (enable)
            mortarAreaRend.transform.localScale = new Vector3(radius, radius, radius);
    }

    public void SetAimPoint(RaycastHit? hit)
    {
        if (hit.HasValue)
        {
            transform.position = hit.Value.point;
            if (oldHitCollider != hit.Value.collider)
            {
                if (hit.Value.collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
                {

                }
                else
                {

                }
            }
            focusedPoint.transform.up = hit.Value.normal;
        }
        else
        {
            transform.position = Vector3.up * 1000;
        }
        oldHitCollider = hit.HasValue ? hit.Value.collider : null;
    }
}