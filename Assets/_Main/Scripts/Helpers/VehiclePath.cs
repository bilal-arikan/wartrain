using System.Collections.Generic;
using UnityEngine;

public class VehiclePath : MonoBehaviour
{
    public List<Transform> path = new List<Transform>();

    public Transform GetClosestPoint(Transform target)
    {
        return target.Closest(path);
    }

    public void OnDrawGizmos()
    {
        if (path.Count > 0)
        {
            for (int i = 0; i < path.Count; i++)
            {
                Gizmos.DrawLine(path[i].position, path[(i + 1) % path.Count].position);
            }
        }
    }
}