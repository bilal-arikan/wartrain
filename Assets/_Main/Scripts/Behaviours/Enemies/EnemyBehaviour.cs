using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public enum EnemyType
{
    Base,
    Vehicle,
    Plane
}
public class EnemyBehaviour : MonoBehaviour
{
    [Serializable]
    public class EnemyFocusedEvent : UnityEvent<bool> { }

    public EnemyType type;
    public float weaponsFireDelay = 5;
    public Vector2 weaponsFireDelayMinMax = new Vector2(2, 5);
    public List<WeaponBehaviour> weapons = new List<WeaponBehaviour>();
    [ShowInInspector, ReadOnly] protected TargetBehaviour focusedTarget;
    [ShowInInspector, ReadOnly] protected Vector3 focusOffset;
    [ShowInInspector, ReadOnly] public TargetBehaviour main { get; protected set; }

    public EnemyFocusedEvent isFocused;

    protected float nextWeaponFireTime;

    protected virtual void Awake()
    {
        main = GetComponent<TargetBehaviour>();
    }
    protected virtual void Start()
    {
        main.onDeath.AddListener(() =>
        {
            SetAwake(false);
        });
        main.onDamaged.AddListener(() =>
        {
            GameView.Instance.ShowTargetHit();
        });
    }
    protected void OnDestroy()
    {
    }
    protected void OnEnable()
    {
    }
    protected void OnDisable()
    {
        SetAwake(false);
    }

    protected void LateUpdate()
    {
        bool fired = false;
        // indicator?.gameObject.SetActive(!main.IsDeath);
        if (main.IsDeath)
            return;
        if (focusedTarget != null && !focusedTarget.IsDeath)
        {
            foreach (var wp in weapons)
            {
                var ready = wp.LookAtUpdateByEnemy(focusedTarget.transform.position + focusOffset);
                if (ready)
                {
                    if (nextWeaponFireTime < Time.time)
                    {
                        fired = fired || wp.TryFire(true);
                    }
                }
            }
        }
        else
        {
            foreach (var wp in weapons)
            {
                var ready = wp.LookAtUpdateDefault();
            }
        }
        if (fired)
        {
            nextWeaponFireTime = Time.time + weaponsFireDelay + UnityEngine.Random.Range(weaponsFireDelayMinMax.x, weaponsFireDelayMinMax.y);
        }
    }

    public void SetAwake(bool awake)
    {
        if (awake && !main.IsDeath)
        {
            nextWeaponFireTime = Time.time + weaponsFireDelay + UnityEngine.Random.Range(weaponsFireDelayMinMax.x, weaponsFireDelayMinMax.y);
            main.SetIndicatorVisible(true);
        }
        else
        {
            main.SetIndicatorVisible(false);
        }
        GameView.Instance?.UpdateUI();
    }

    [Button]
    public virtual void SetFocusedTarget(TargetBehaviour focusedTarget, Vector3 focusOffset)
    {
        weapons.Clear();
        weapons.AddRange(GetComponentsInChildren<WeaponBehaviour>());
        weapons.ForEach(w => w.angelIndicator.gameObject.SetActive(false));
        weapons.ForEach(w => w.trajectoryLine.gameObject.SetActive(false));

        this.focusedTarget = focusedTarget;
        this.focusOffset = focusOffset;

        if (focusedTarget != null)
        {
            SetAwake(true);
            isFocused.Invoke(true);
        }
        else
        {
            SetAwake(false);
            isFocused.Invoke(false);
        }
    }

    protected void OnTriggerEnter(Collider other)
    {
        var vagon = other.attachedRigidbody?.GetComponent<TargetBehaviour>();
        if (vagon != null && vagon.team == Team.Player)
        {
            SetFocusedTarget(vagon, other.bounds.center - vagon.transform.position);
        }
    }
    protected void OnTriggerExit(Collider other)
    {
        if (focusedTarget == other.attachedRigidbody?.transform)
        {
            SetFocusedTarget(null, Vector3.zero);
        }
    }
    // protected void OnDrawGizmos()
    // {
    //     if (focusedTarget)
    //     {
    //         Gizmos.color = Color.red;
    //         Gizmos.DrawWireSphere(focusedTarget.transform.position + focusOffset, 5);
    //     }
    // }

}