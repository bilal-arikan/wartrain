using System;
using Arikan;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;

public class SettingsController : SingletonBehaviour<SettingsController>
{
    public AudioListener listener;
    public Volume postProcessingVolume;

    [ShowInInspector]
    public DataController ints => DataController.Instance;

    private void Start()
    {

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SettingsView.Instance.Open();
        }
        if (Input.GetKeyDown(KeyCode.DoubleQuote))
        {
            SRDebug.Instance.ShowDebugPanel();
        }
    }

    internal void SetLeftHand(bool v)
    {
        DataController.Sett_LeftHand.Value = v;
        SettingsView.Instance.UpdateUI();
    }

    internal void SetVibration(bool v)
    {
        DataController.Sett_Vibration.Value = v;
        SettingsView.Instance.UpdateUI();
    }

    internal void SetSound(bool v)
    {
        DataController.Sett_Sound.Value = v;
        SettingsView.Instance.UpdateUI();
    }

    internal void SetMusic(bool v)
    {
        DataController.Sett_Music.Value = v;
        SettingsView.Instance.UpdateUI();
    }

    internal void SetGraphics(int v)
    {
        DataController.Sett_Graphics.Value = v;
        QualitySettings.SetQualityLevel(v == 0 ? 0 : 2, true);
        SettingsView.Instance.UpdateUI();
    }

    internal bool GetLeftHand() => DataController.Sett_LeftHand;
    internal bool GetVibration() => DataController.Sett_Vibration;
    internal bool GetSound() => DataController.Sett_Sound;
    internal bool GetMusic() => DataController.Sett_Music;
    internal int GetGraphics() => DataController.Sett_Graphics;
}