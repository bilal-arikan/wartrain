using System;
using System.Collections;
using System.Collections.Generic;
using Arikan;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FinishView : UIView<FinishView>
{
    public Button startButton;
    public Button watchAdButton;
    public Button toMenuButton;
    [Header("Rewards")]
    public Text moneyText;
    public Text goldText;
    public Text xpText;

    public override string Group => "main";

    void Start()
    {
        startButton.onClick.AddListener(GameController.Instance.NextLevel);
        toMenuButton.onClick.AddListener(GameController.Instance.ToMenu);
        watchAdButton.onClick.AddListener(WatchAd);
        toMenuButton.onClick.AddListener(Close);
    }

    private void WatchAd()
    {
        AdsSystem.Instance.ShowReward((s) =>
        {
            if (s)
            {
            }
        });
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        // DataController.Money.OnBeforeChanged += (v1, v2) => moneyText.Counter(v1, v2, 0.5f, 0.1f, format: "${0}").RunAsPromise(this);
    }

    public void Parse(int money)
    {
        moneyText.text = ": " + money;
        goldText.text = ": ";
        xpText.text = ": ";
    }
}
