using System;
using System.Collections.Generic;
using Arikan;
using Sirenix.OdinInspector;
using UnityEngine;

public class CustomizeController : SingletonBehaviour<CustomizeController>
{
    [ShowInInspector] public List<CustomizeItem> customizeItems => CustomizeResources.Instance.customizeItems;


    private const int nonPurchasedValue = 0;
    private const int purchasedValue = 1;
    private const int appliedValue = 2;

    private void Start()
    {

    }
    // Is Item Owned
    [Button]
    public bool IsItemOwned(string id)
    {
        var value = PlayerPrefs.GetInt($"Item_{id}");
        return value == purchasedValue || value == appliedValue;
    }
    // Buy Item
    [Button]
    public void BuyItem(string id, Action<bool> callback)
    {
        if (DataController.Money >= GetItem(id).price)
        {
            DataController.Money.Value -= GetItem(id).price;
            PlayerPrefs.SetInt($"Item_{id}", purchasedValue);
            ApplyItem(id, null);
            callback?.Invoke(true);
            PurchaseSuccessView.Instance.Open();
            Debug.Log("BuyItem " + id);
        }
        else
        {
            callback?.Invoke(false);
            PurchaseFailedView.Instance.Open();
        }

    }
    // Give Item
    [Button]
    public void GiveItem(string id, bool apply = false)
    {
        PlayerPrefs.SetInt($"Item_{id}", purchasedValue);
        if (apply || GetAppliedItem(GetItem(id).ctgry) == null)
            ApplyItem(id, null);
    }
    // Apply Item
    [Button]
    public void ApplyItem(string id, Action<bool> callback)
    {
        if (IsItemOwned(id))
        {
            var item = GetItem(id);

            if (item is CustomizeItemWeapon)
            {
                PlayerController.Instance.ApplyItem(item as CustomizeItemWeapon);
            }
            else if (item is CustomizeItemVagon)
            {
                PlayerController.Instance.ApplyItem(item as CustomizeItemVagon);
            }

            foreach (var itemOwned in GetOwnedItems(item.ctgry))
            {
                PlayerPrefs.SetInt($"Item_{itemOwned.id}", itemOwned.id == id ? appliedValue : purchasedValue);
            }
            Debug.Log("ApplyItem " + id);
            callback?.Invoke(true);
        }
        else
        {
            callback?.Invoke(false);
        }
    }
    // Is item Applied
    [Button]
    public bool IsItemApplied(string id)
    {
        return PlayerPrefs.GetInt($"Item_{id}") == appliedValue;
    }
    // Get Item
    [Button]
    public CustomizeItem GetItem(string id)
    {
        foreach (var i in customizeItems)
        {
            if (i.id == id)
            {
                return i;
            }
        }
        return null;
    }
    // Get All Owned Items
    [Button]
    public List<CustomizeItem> GetOwnedItems()
    {
        List<CustomizeItem> ownedItems = new List<CustomizeItem>();
        foreach (var i in customizeItems)
        {
            if (IsItemOwned(i.id))
            {
                ownedItems.Add(i);
            }
        }
        return ownedItems;
    }
    // Get All Owned Items By Category
    [Button]
    public List<CustomizeItem> GetOwnedItems(string ctgry)
    {
        List<CustomizeItem> ownedItems = new List<CustomizeItem>();
        foreach (var i in GetOwnedItems())
        {
            if (i.ctgry == ctgry)
            {
                ownedItems.Add(i);
            }
        }
        return ownedItems;
    }
    // Get all applied items by category
    [Button]
    public List<CustomizeItem> GetAppliedItems()
    {
        List<CustomizeItem> appliedItems = new List<CustomizeItem>();
        foreach (var i in GetOwnedItems())
        {
            if (IsItemApplied(i.id))
            {
                appliedItems.Add(i);
            }
        }
        return appliedItems;
    }
    // Get applied item by category
    [Button]
    public CustomizeItem GetAppliedItem(string ctgry)
    {
        foreach (var i in GetAppliedItems())
        {
            if (i.ctgry == ctgry)
            {
                return i;
            }
        }
        return null;
    }
}