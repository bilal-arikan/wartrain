﻿//***********************************************************************//
// Copyright (C) 2017 Bilal Arikan. All Rights Reserved.
// Author: Bilal Arikan
// Time  : 27.11.2018   
//***********************************************************************//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using UnityEngine;

/// <summary>
/// Add() ve RemoveAt() methodları doğrudan PlayerPrefse kaydeder
/// </summary>
/// <typeparam name="TKey"></typeparam>
/// <typeparam name="TValue"></typeparam>
[Serializable]
public class List_<TValue> : IEnumerable<TValue>, IEnumerable, IPrefObject
{
    const string toStringFormat = "000000";

    [field: SerializeField]
    public string Key { get; protected set; }

    protected readonly List<int> UniqeIds;
    protected readonly List<TValue> Values;

    [NonSerialized]
    public Action OnOnlyChanged;


    [Newtonsoft.Json.JsonConstructor]
    public List_(IEnumerable<TValue> values)
    {
        //Field = typeof(Database).GetFields().Find(f => f.GetValue(Database.Instance) == this);
        //Debug.Log("List_ 2 " + Uniqekey);
        UniqeIds = JsonConvert.DeserializeObject<List<int>>(PlayerPrefs.GetString(Key + "Ids", "[]"));
        Values = new List<TValue>();
        Clear();
        if (values != null)
            AddRange(values);
    }


    /// <summary>
    /// </summary>
    /// <param name="prefsUniqeKey">
    /// Diğer List lerden ayırması için Uniqe Key
    /// </param>
    public List_(string key, params TValue[] dfults)
    {
        Key = "LP_" + key + "_";
        //Field = typeof(Database).GetFields().Find(f => f.GetValue(Database.Instance) == this);
        //Debug.Log("List_ 1 " + Uniqekey);

        if (PlayerPrefs.HasKey(Key + "Ids"))
        {
            UniqeIds = JsonConvert.DeserializeObject<List<int>>(PlayerPrefs.GetString(Key + "Ids", "[]"));
            Values = new List<TValue>(UniqeIds.Count);
            for (int i = 0; i < UniqeIds.Count; i++)
            {
                //if (!typeof(TValue).IsPrimitive && typeof(TValue) != typeof(string))
                //    Debug.Log("###" + JsonConvert.DeserializeObject<TValue>(PlayerPrefs.GetString(Uniqekey + UniqeIds[i].ToString(toStringFormat))));
                Values.Add(JsonConvert.DeserializeObject<TValue>(PlayerPrefs.GetString(Key + UniqeIds[i].ToString(toStringFormat))));
            }
            //Debug.Log("ListPrefs 1");
        }
        else
        {
            UniqeIds = new List<int>(dfults?.Length ?? 0);
            PlayerPrefs.SetString(Key + "Ids", JsonConvert.SerializeObject(UniqeIds));
            Values = new List<TValue>(dfults?.Length ?? 0);
            if (dfults != null)
                AddRange(dfults);
            //Debug.Log("ListPrefs 2");
        }
        //Debug.Log(PrefsUniqeKey + JsonConvert.SerializeObject(Values));
    }
    public List<TValue> GetValueList()
    {
        List<TValue> temp = new List<TValue>();
        for (int i = 0; i < Values.Count; i++)
        {
            temp.Add(Values[i]);
        }
        return temp;
    }
    public TValue this[int index]
    {
        get { return Values[index]; }
        set
        {
            Values[index] = value;
            PlayerPrefs.SetString(Key + UniqeIds[index].ToString(toStringFormat), JsonConvert.SerializeObject(value));
            OnOnlyChanged?.Invoke();
        }
    }

    public int Count => UniqeIds.Count;

    public bool IsReadOnly => false;
    public void Add(TValue item)
    {
        //Debug.Log("List_ 3 " + Count);

        int newId = NewUniqeId(item);
        UniqeIds.Add(newId);
        PlayerPrefs.SetString(Key + "Ids", JsonConvert.SerializeObject(UniqeIds));
        Values.Add(item);
        PlayerPrefs.SetString(Key + newId.ToString(toStringFormat), JsonConvert.SerializeObject(item));
        OnOnlyChanged?.Invoke();
    }

    public void AddRange(IEnumerable<TValue> items)
    {
        //Debug.Log("List_ 4 " + items.Count());
        foreach (var item in items)
            Add(item);
    }
    public void Clear()
    {
        //PlayerPrefs.DeleteKey(Uniqekey + "Ids");
        PlayerPrefs.SetString(Key + "Ids", "[]");
        foreach (var id in UniqeIds)
        {
            PlayerPrefs.DeleteKey(Key + id.ToString(toStringFormat));
        }
        UniqeIds.Clear();
        Values.Clear();
        OnOnlyChanged?.Invoke();
    }

    public bool Contains(TValue item)
    {
        return Values.Contains(item);
    }

    public void CopyTo(TValue[] array, int arrayIndex = 0)
    {
        Values.CopyTo(array, arrayIndex);
    }
    public IEnumerator<TValue> GetEnumerator()
    {
        return Values.GetEnumerator();
    }

    public int IndexOf(TValue item)
    {
        return Values.IndexOf(item);
    }

    public void Insert(int index, TValue item)
    {
        int newId = NewUniqeId(item);
        Values.Insert(index, item);
        PlayerPrefs.SetString(Key + newId.ToString(toStringFormat), JsonConvert.SerializeObject(item));
        UniqeIds.Insert(index, newId);
        PlayerPrefs.SetString(Key + "Ids", JsonConvert.SerializeObject(UniqeIds));
        OnOnlyChanged?.Invoke();
    }

    public bool Remove(TValue item)
    {
        int index = Values.IndexOf(item);
        if (index > -1)
        {
            RemoveAt(index);
            Values.RemoveAt(index);
            OnOnlyChanged?.Invoke();
            return true;
        }
        return false;
    }

    public void RemoveAt(int index)
    {
        Values.RemoveAt(index);
        PlayerPrefs.DeleteKey(Key + UniqeIds[index].ToString(toStringFormat));
        UniqeIds.RemoveAt(index);
        PlayerPrefs.SetString(Key + "Ids", JsonConvert.SerializeObject(UniqeIds));
        OnOnlyChanged?.Invoke();
    }

    public object GetValue()
    {
        return Values;
    }
    public object GetDefaultValue()
    {
        return new List<TValue>();
    }
    public void Save()
    {
        PlayerPrefs.SetString(Key + "Ids", JsonConvert.SerializeObject(UniqeIds));
        for (int i = 0; i < Values.Count; i++)
            PlayerPrefs.SetString(Key + UniqeIds[i].ToString(toStringFormat), JsonConvert.SerializeObject(Values[i]));
        PlayerPrefs.Save();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return Values.GetEnumerator();
    }

    int NewUniqeId(TValue obj)
    {
        int hash = obj.GetHashCode();
        if (!UniqeIds.Contains(hash))
            return hash;
        int tryCount = 0;
        while (tryCount < 1000) //Sonsuz döngüye düşmemesi için
        {
            var newValue = UnityEngine.Random.Range(0, 999999);
            if (!UniqeIds.Contains(newValue))
                return newValue;
            tryCount++;
        }
        return -1;
    }
}

public static class ListPrefsExt
{
    public static TObj Find<TObj>(this List_<TObj> l, Predicate<TObj> match)
    {
        foreach (var item in l)
        {
            if (match(item))
                return item;
        }
        return default(TObj);
    }
    public static int FindIndex<TObj>(this List_<TObj> l, Predicate<TObj> match)
    {
        int ind = -1;
        foreach (var item in l)
        {
            ind++;
            if (match(item))
                return ind;
        }
        return -1;
    }
    public static TObj[] FindAll<TObj>(this List_<TObj> l, Predicate<TObj> match)
    {
        List<TObj> temp = new List<TObj>();
        foreach (var item in l)
        {
            if (match(item))
                temp.Add(item);
        }
        return temp.ToArray();
    }

    public static bool Exists<TObj>(this List_<TObj> l, Func<TObj, bool> p)
    {
        foreach (var v in l)
            if (p(v))
                return true;
        return false;
    }

}