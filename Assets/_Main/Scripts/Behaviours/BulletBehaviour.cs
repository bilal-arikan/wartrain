using System.Runtime.Serialization.Formatters;
using DG.Tweening;
using Sirenix.OdinInspector;
using ToolBox.Pools;
using UnityEngine;
using UnityEngine.Events;

public class BulletBehaviour : SerializedMonoBehaviour//, IPoolable
{
    public Rigidbody body;
    public Transform explosionPoint;
    public int damage = 1;
    public float bulletSpeed = 200f;
    public float areaDamageRadius = 0f;
    public float activateColliderDelay = 0.1f;
    public float autoDestroyDelay = 4f;
    public ParticleSystem explosionParticle;

    public UnityEvent onHit;

    private new Collider collider;
    private bool arrived;
    private float spawnTime;
    private float lifeTime => Time.time - spawnTime;

    private static RaycastHit[] areaHits;
    public const int maxAreaDamageObjects = 5;
    private Tween removeToPoolTween;
    private ParticleSystem spawnedExplosionParticle;

    private void Awake()
    {
        collider = GetComponent<Collider>();
        // explosionParticle.gameObject.Populate(1);
    }

    private void OnEnable()
    {
        collider.enabled = false;
        spawnTime = Time.time;
        arrived = false;
        if (areaHits == null)
        {
            areaHits = new RaycastHit[maxAreaDamageObjects];
        }
        // var error = new Vector2(
        //     Mathf.Lerp(-10, 10, UnityEngine.Random.value),
        //     Mathf.Lerp(-10, 10, UnityEngine.Random.value)
        // ) * (1 - accuracy);  
        body.velocity = transform.forward * bulletSpeed;
        body.angularVelocity = Vector3.zero;

        this.Invoke(() => collider.enabled = true, activateColliderDelay);
    }
    public void TimedDisable()
    {
        if (removeToPoolTween != null) removeToPoolTween.Kill();
        removeToPoolTween = DOVirtual.DelayedCall(autoDestroyDelay, () => OnHit(null));
    }

    private void OnCollisionEnter(Collision other)
    {
        if (arrived)
            return;

        spawnedExplosionParticle = explosionParticle.gameObject.Get<ParticleSystem>(explosionPoint.position, Quaternion.identity);
        spawnedExplosionParticle.Stop(true);
        spawnedExplosionParticle.Clear(true);
        spawnedExplosionParticle.Play(true);

        DOVirtual.DelayedCall(3, spawnedExplosionParticle.gameObject.Release);

        OnHit(other.collider);
    }

    public void MuliplySpeed(float mul)
    {
        body.velocity = transform.forward * bulletSpeed * mul;
    }

    void OnHit(Collider other)
    {
        arrived = true;
        if (other)
        {
            var target = other.GetComponentInParent<TargetBehaviour>();
            if (areaDamageRadius > 0f)
            {
                var count = Physics.SphereCastNonAlloc(transform.position, areaDamageRadius, transform.forward, areaHits, areaDamageRadius * 2, GameResources.Instance.bulletAreaHitMask);
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (areaHits[i].rigidbody && areaHits[i].rigidbody.TryGetComponent<TargetBehaviour>(out var areaTarget))
                        {
                            areaTarget.OnHitBy(this);
                            Debug.Log("Area Hit " + other.name, other);
                        }
                    }
                }
            }
            else if (lifeTime > 0.05f && target != null)
            {
                target.OnHitBy(this);
            }
            // Debug.Log("Hit " + other.name, other);
            onHit.Invoke();
        }
        gameObject.Release();
        if (removeToPoolTween != null) removeToPoolTween.Kill();
    }


}