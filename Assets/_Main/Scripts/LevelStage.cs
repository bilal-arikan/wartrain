using System;
using System.Collections.Generic;
using System.Linq;
using BezierSolution;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class LevelStage : SerializedMonoBehaviour
{
    public Collider stageFireCollider;
    public Dictionary<SpawnPoint, GameObject> spawnPoints;
    public Dictionary<BezierSpline, BezierWalkerWithSpeed> spawnPaths;

    [ShowInInspector, ReadOnly] public List<TargetBehaviour> spawnedObjectsByStage = new List<TargetBehaviour>();

    [ShowInInspector, ReadOnly] public bool IsAlarmed { get; private set; }
    [ShowInInspector, ReadOnly] public bool IsCompleted { get; private set; }
    public UnityEvent onActivated;
    public UnityEvent onCompleted;

    private void Awake()
    {
        var objs = gameObject.GetDescendants().Select(o => o.GetComponent<TargetBehaviour>()).Where(o => o != null).ToList();
        foreach (var item in objs)
        {
            spawnedObjectsByStage.Add(item);
            item.onDeath.AddListener(() => OnEnemyDestroyed(item));
            // Debug.Log("Sp " + obj.name, obj);
        }
        // SetEnableEnemyWeapons(false);
    }
    private void OnValidate()
    {
        var paths = GetComponentsInChildren<BezierSpline>();
        var points = GetComponentsInChildren<SpawnPoint>();
        for (int j = 0; j < paths.Length; j++)
            spawnPaths.AddOrDontChange(paths[j], null);
        for (int k = 0; k < points.Length; k++)
            spawnPoints.AddOrDontChange(points[k], null);
    }
    private void OnEnable()
    {
        IsCompleted = false;
        IsAlarmed = false;
        onActivated.Invoke();
    }
    [Button]
    public void AlarmStage()
    {
        IsAlarmed = true;
        var train = PlayerController.Instance.currentTrain;
        for (int i = 0; i < spawnedObjectsByStage.Count; i++)
        {
            if (spawnedObjectsByStage[i].TryGetComponent<EnemyBehaviour>(out var enemy))
            {
                enemy.SetFocusedTarget(train.main, train.visualObject.boundsColl.bounds.center - train.transform.position);
            }
        }

        if (spawnedObjectsByStage.All(o => o.IsDeath))
        {
            OnEnemyDestroyed(spawnedObjectsByStage.FirstOrDefault());
        }
    }

    public void OnEnemyDestroyed(TargetBehaviour enemy)
    {
        if (!IsCompleted && IsAlarmed && spawnedObjectsByStage.All(o => o.IsDeath))
        {
            IsCompleted = true;
            Debug.Log("Stage " + name + " completed");
            GetComponentsInChildren<GateWayBehaviour>().ForEach(o => o.Open(0f));

            onCompleted.Invoke();
            LevelController.Instance.NextStage();
        }
    }
}