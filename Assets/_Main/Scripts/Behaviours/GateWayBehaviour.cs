using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public enum GateOpenType
{
    Manuel,
    Timer,
    StageClear,
    DestroyTarget
}

public class GateWayBehaviour : MonoBehaviour
{
    public GateOpenType openType;
    public bool isFinishGate;
    public float openTimerDuration = 6f;
    public Transform leftPlank;
    public Transform rightPlank;

    public UnityEvent onOpened;

    [ShowInInspector, ReadOnly] public bool IsOpened { get; private set; }
    [ShowInInspector, ReadOnly] public TrainBehaviour activator { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        IsOpened = false;

    }

    public void ActivateBy(TrainBehaviour train)
    {
        if (IsOpened)
            return;
        if (activator != null)
            return;
        activator = train;
        LevelController.Instance.currentGateWay = this;
        Debug.Log("GateWay ActivateBy " + this, this);
        switch (openType)
        {
            case GateOpenType.Manuel:
                break;
            case GateOpenType.Timer:
                DOVirtual.DelayedCall(openTimerDuration, Opened);
                break;
            case GateOpenType.StageClear:
                break;
            case GateOpenType.DestroyTarget:
                break;
        }
        GameView.Instance.UpdateUI();
    }
    [Button]
    public void Open(float animationTime = 1f)
    {
        DOVirtual.DelayedCall(animationTime, Opened);
    }
    public void Opened()
    {
        if (IsOpened)
            return;
        Debug.Log("GateWay Opened " + this, this);
        onOpened.Invoke();
        IsOpened = true;

        if (leftPlank)
            leftPlank.DOLocalRotate(new Vector3(0, 0, 90), 1.5f).SetEase(Ease.InOutSine);
        if (rightPlank)
            rightPlank.DOLocalRotate(new Vector3(0, 0, -90), 1.5f).SetEase(Ease.InOutSine);

        GameView.Instance.UpdateUI();
        if (isFinishGate)
        {
            GameController.Instance.FinishLevel();
        }
    }
}
