using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class MarketView : UIView<MarketView>
{
    public Button closeButton;
    public MarketItem itemPrefab;
    public Dictionary<string, LayoutGroup> itemsLayoutGroup;

    public override string Group => "";
    private List<MarketItem> _items = new List<MarketItem>();

    protected override void Awake()
    {
        base.Awake();
        closeButton.onClick.AddListener(Close);
        var allItems = CustomizeController.Instance.customizeItems;
        foreach (var item in allItems)
        {
            var newItem = Instantiate(itemPrefab, itemsLayoutGroup[item.ctgry].transform);
            // newItem.toggle.group = itemsLayoutGroup[item.ctgry].gameObject.GetOrAddComponent<ToggleGroup>();
            // newItem.toggle.group.allowSwitchOff = true;
            newItem.Init(item);
            _items.Add(newItem);
        }

    }
    // Start is called before the first frame update
    void Start()
    {

        UpdateUI();
    }

    protected override void OnEnable()
    {
        UpdateUI();
        base.OnEnable();
        DG.Tweening.DOVirtual.DelayedCall(0.01f, () =>
        {
            foreach (var layoKV in itemsLayoutGroup)
            {
                layoKV.Value.padding = layoKV.Value.padding;
                layoKV.Value.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
                LayoutRebuilder.ForceRebuildLayoutImmediate(layoKV.Value.GetComponent<RectTransform>());
            }
            Canvas.ForceUpdateCanvases();
        });
    }
    protected override void OnDisable()
    {
        base.OnDisable();
    }

    public override void UpdateUI()
    {
        foreach (var item in _items)
        {
            item.UpdateUI();
        }
        Debug.Log("MarketView UpdateUI");
    }
}
