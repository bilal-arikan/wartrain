using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Composites;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;
using UnityEngine.Serialization;
using UnityEngine.UI;

[AddComponentMenu("Input/OnScreenPointerDown")]
public class OnScreenPointerDown : OnScreenControl, IPointerDownHandler, IPointerUpHandler
{
    private Graphic graphic;

    public AxisComposite comp;

    [InputControl(layout = "Axis")]
    [SerializeField]
    private string m_ControlPath;

    protected override string controlPathInternal
    {
        get => m_ControlPath;
        set => m_ControlPath = value;
    }

    private void Awake()
    {
        graphic = GetComponent<Graphic>();
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        SendValueToControl(1f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        SendValueToControl(0f);
    }
}