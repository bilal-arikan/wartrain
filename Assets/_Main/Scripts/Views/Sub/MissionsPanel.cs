using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class MissionsPanel : UIView<MissionsPanel>
{
    public override string Group => "";
    public Button OpenPanelButton;
    public Button ClosePanelButton;
    public GameObject MissionsPanelRoot;
    public LayoutGroup MissionsContent;

    protected override void Awake()
    {
        base.Awake();
        OpenPanelButton.onClick.AddListener(() =>
        {
            MissionsPanelRoot.SetActive(true);
            OpenPanelButton.gameObject.SetActive(false);
            ClosePanelButton.gameObject.SetActive(true);
        });
        ClosePanelButton.onClick.AddListener(() =>
        {
            MissionsPanelRoot.SetActive(false);
            OpenPanelButton.gameObject.SetActive(true);
            ClosePanelButton.gameObject.SetActive(false);
        });
        MissionsPanelRoot.SetActive(false);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
    }
}