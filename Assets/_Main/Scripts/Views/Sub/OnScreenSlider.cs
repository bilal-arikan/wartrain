using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Composites;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;
using UnityEngine.UI;

[AddComponentMenu("Input/OnScreenSlider")]
public class OnScreenSlider : OnScreenControl
{
    private Slider slider;

    public AxisComposite comp;
    public bool receiveInputValue;
    [InputControl(layout = "Axis")]
    [SerializeField]
    private string m_ControlPath;

    protected override string controlPathInternal
    {
        get => m_ControlPath;
        set => m_ControlPath = value;
    }

    private void Awake()
    {
        slider = GetComponent<Slider>();
        slider.onValueChanged.AddListener(OnValueChanged);
    }
    private void Update()
    {
        if (slider.interactable)
        {
            SendValueToControl(slider.value);
        }
    }

    void OnValueChanged(float v)
    {
        SendValueToControl(v);
    }
}