using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class PausedView : UIView<PausedView>
{
    public Button closeButton;
    public Button toMenuButton;
    public Button settingsButton;

    public override string Group => "";

    void Start()
    {
        closeButton.onClick.AddListener(Close);
        toMenuButton.onClick.AddListener(GameController.Instance.ToMenu);
        toMenuButton.onClick.AddListener(Close);
        settingsButton.onClick.AddListener(SettingsView.Instance.Open);
        // settingsButton.onClick.AddListener(Close);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        Time.timeScale = 0;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        Time.timeScale = 1;
    }
}
