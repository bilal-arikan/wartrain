using UnityEngine;

[CreateAssetMenu(fileName = "CustomizeItemWeapon", menuName = "GDC_WarTrain/CustomizeItemWeapon", order = 0)]
public class CustomizeItemWeapon : CustomizeItem
{
    public WeaponBehaviour weaponBehaviour;
    public override string ctgry => weaponBehaviour?.weaponCtgry.ToString();
    public override UnityEngine.Object asset => weaponBehaviour;
}