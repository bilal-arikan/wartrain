using System.Collections;
using System.Collections.Generic;
using Arikan;
using SRDebugger;
using UnityEngine;
using UnityEngine.UI;

public class SettingsView : UIView<SettingsView>
{
    public Button closeButton;

    public Button[] leftHandBtn;
    public Button[] vibraBtn;
    public Button[] soundBtn;
    public Button[] musicBtn;
    public Button[] graphicsBtn;


    public override string Group => "";

    void Start()
    {
        closeButton.onClick.AddListener(Close);
        leftHandBtn[0].onClick.AddListener(() => SettingsController.Instance.SetLeftHand(false));
        leftHandBtn[1].onClick.AddListener(() => SettingsController.Instance.SetLeftHand(true));
        vibraBtn[0].onClick.AddListener(() => SettingsController.Instance.SetVibration(false));
        vibraBtn[1].onClick.AddListener(() => SettingsController.Instance.SetVibration(true));
        soundBtn[0].onClick.AddListener(() => SettingsController.Instance.SetSound(false));
        soundBtn[1].onClick.AddListener(() => SettingsController.Instance.SetSound(true));
        musicBtn[0].onClick.AddListener(() => SettingsController.Instance.SetMusic(false));
        musicBtn[1].onClick.AddListener(() => SettingsController.Instance.SetMusic(true));
        graphicsBtn[0].onClick.AddListener(() => SettingsController.Instance.SetGraphics(0));
        graphicsBtn[1].onClick.AddListener(() => SettingsController.Instance.SetGraphics(1));
    }
    protected override void OnEnable()
    {
        base.OnEnable();
        UpdateUI();
    }

    public override void UpdateUI()
    {
        leftHandBtn[0].interactable = SettingsController.Instance.GetLeftHand() == true;
        leftHandBtn[1].interactable = SettingsController.Instance.GetLeftHand() == false;
        vibraBtn[0].interactable = SettingsController.Instance.GetVibration() == true;
        vibraBtn[1].interactable = SettingsController.Instance.GetVibration() == false;
        soundBtn[0].interactable = SettingsController.Instance.GetSound() == true;
        soundBtn[1].interactable = SettingsController.Instance.GetSound() == false;
        musicBtn[0].interactable = SettingsController.Instance.GetMusic() == true;
        musicBtn[1].interactable = SettingsController.Instance.GetMusic() == false;
        graphicsBtn[0].interactable = SettingsController.Instance.GetGraphics() == 1;
        graphicsBtn[1].interactable = SettingsController.Instance.GetGraphics() == 0;
    }
}
