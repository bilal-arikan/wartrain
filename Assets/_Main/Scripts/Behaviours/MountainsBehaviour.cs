using Arikan;
using Sirenix.OdinInspector;
using UnityEngine;

public class MountainsBehaviour : SingletonBehaviour<MountainsBehaviour>
{
    public Transform[] spawnPoints;

    private MeshRenderer[] spawnedMountains;

    [Button]
    public void SpawnMountains(MeshRenderer mountainPrefab)
    {
        ClearMountains();
        if (mountainPrefab == null)
            return;
        spawnedMountains = new MeshRenderer[spawnPoints.Length];
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            spawnedMountains[i] = Instantiate(mountainPrefab, spawnPoints[i]);
            spawnedMountains[i].transform.localPosition = Vector3.zero;
            spawnedMountains[i].transform.localRotation = Quaternion.identity;
        }
    }
    [Button]
    public void ClearMountains()
    {
        if (spawnedMountains != null)
        {
            for (int i = 0; i < spawnedMountains.Length; i++)
            {
                if (spawnedMountains[i])
                {
                    DestroyImmediate(spawnedMountains[i].gameObject);
                }
            }
        }
    }
}