using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class ThrowOnExplosion : MonoBehaviour
{
    public bool throwOnDeath = true;
    public List<Transform> throwObjects = new List<Transform>();
    public float setAllKinematicDelay = 10;
    public Vector2 throwMaxForce = new Vector2(5, 15);
    public Vector2 throwMinForce = new Vector2(3, 10);
    public float rotationSpeed = 20;

    private void Start()
    {
        if (throwOnDeath)
        {
            GetComponentInParent<TargetBehaviour>().onDeath.AddListener(ThrowRandom);
        }
    }

    [Button]
    public void ThrowRandom()
    {
        foreach (var tr in throwObjects)
        {
            var rb = tr.gameObject.GetOrAddComponent<Rigidbody>();
            rb.gameObject.GetOrAddComponent<BoxCollider>();
            rb.isKinematic = false;
            rb.mass = rb.mass < 2f ? 50 : rb.mass;
            rb.AddForce(
                new Vector3(
                    Random.Range(-throwMaxForce.x, throwMaxForce.x),
                    Random.Range(throwMaxForce.y, throwMinForce.y),
                    Random.Range(-throwMaxForce.x, throwMaxForce.x)),
                ForceMode.Impulse);
            rb.angularVelocity = Random.onUnitSphere * rotationSpeed;
        }
        // DOVirtual.DelayedCall(setAllKinematicDelay, SetAllKinematic);
    }

    void SetAllKinematic()
    {
        foreach (var tr in throwObjects)
        {
            var rb = tr.GetComponent<Rigidbody>();
            rb.isKinematic = true;
        }
    }
}