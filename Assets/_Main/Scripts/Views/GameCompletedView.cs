using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class GameCompletedView : UIView<GameCompletedView>
{
    public Button closeButton;
    public GameObject comingSoon;

    public override string Group => "";

    void Start()
    {
        closeButton.onClick.AddListener(Close);
    }

    protected override void OnDisable()
    {
        comingSoon.SetActive(false);
        base.OnDisable();
    }

    public void OpenWitnComingSoon()
    {
        comingSoon.SetActive(true);
        base.Open(true);
    }
}
