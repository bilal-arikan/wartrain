﻿////***********************************************************************//
//// Copyright (C) 2017 Bilal Arikan. All Rights Reserved.
//// Author: Bilal Arikan
//// Time  : 27.11.2018   
////***********************************************************************//
//using Genetic;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using UnityEngine;

///// <summary>
///// Add() ve RemoveAt() methodları doğrudan PlayerPrefse kaydeder
///// </summary>
///// <typeparam name="TKey"></typeparam>
///// <typeparam name="TValue"></typeparam>
//[Serializable]
//public class HashSet_<TValue> : ICollection<TValue>, IEnumerable<TValue>, ISet<TValue>, IEnumerable, IPrefsObject
//{
//    public const bool DirectUseForPrimitives = false;

//    const string toStringFormat = "000000";

//    public string Uniqekey;
//    public readonly List<int> UniqeIds;
//    public readonly List<TValue> Values;

//    [NonSerialized]
//    public Action OnChanged;

//    public HashSet_(){

//    }

//    /// <summary>
//    /// </summary>
//    /// <param name="prefsUniqeKey">
//    /// Diğer List lerden ayırması için Uniqe Key
//    /// </param>
//    //[Newtonsoft.Json.JsonConstructor]
//    public HashSet_(string prefsUniqeKey, params TValue[] dfults)
//    {
//        Uniqekey = "LP_" + prefsUniqeKey + "_";

//        if (PlayerPrefs.HasKey(Uniqekey + "Ids"))
//        {
//            UniqeIds = JsonConvert.DeserializeObject<List<int>>(PlayerPrefs.GetString(Uniqekey + "Ids", "[]"));
//            Values = new List<TValue>(UniqeIds.Count);
//            for (int i = 0; i < UniqeIds.Count; i++)
//            {
//                Values.Add(JsonConvert.DeserializeObject<TValue>(PlayerPrefs.GetString(Uniqekey + UniqeIds[i].ToString(toStringFormat))));
//            }
//            //Debug.Log("ListPrefs 1");
//        }
//        else
//        {
//            UniqeIds = new List<int>(dfults?.Length ?? 0);
//            PlayerPrefs.SetString(Uniqekey + "Ids", JsonConvert.SerializeObject(UniqeIds));
//            Values = new List<TValue>(dfults?.Length ?? 0);
//            if(dfults != null)
//                foreach (var df in dfults)
//                    Add(df);
//            //Debug.Log("ListPrefs 2");
//        }
//        //Debug.Log(PrefsUniqeKey + JsonConvert.SerializeObject(Values));
//    }

//    public TValue this[int index]
//    {
//        get { return Values[index]; }
//        set {
//            Values[index] = value;
//            PlayerPrefs.SetString(Uniqekey + UniqeIds[index].ToString(toStringFormat), JsonConvert.SerializeObject(value));
//            OnChanged?.Invoke();
//        }
//    }

//    public int Count => UniqeIds.Count;

//    public bool IsReadOnly => false;

//    void ICollection<TValue>.Add(TValue item) => Add(item);
//    bool ISet<TValue>.Add(TValue item) => Add(item);
//    public bool Add(TValue item)
//    {
//        if (Values.Contains(item))
//            return false;
//        int newId = NewUniqeId(item);
//        UniqeIds.Add(newId);
//        PlayerPrefs.SetString(Uniqekey + "Ids", JsonConvert.SerializeObject(UniqeIds));
//        Values.Add(item);
//        PlayerPrefs.SetString(Uniqekey + newId.ToString(toStringFormat), JsonConvert.SerializeObject(item));
//        OnChanged?.Invoke();
//        return true;
//    }

//    public void AddRange(IEnumerable<TValue> items)
//    {
//        foreach (var item in items)
//            Add(item);
//    }
//    public void Clear()
//    {
//        PlayerPrefs.DeleteKey(Uniqekey + "Ids");
//        foreach (var id in UniqeIds)
//        {
//            PlayerPrefs.DeleteKey(Uniqekey + id.ToString(toStringFormat));
//        }
//        UniqeIds.Clear();
//        Values.Clear();
//        OnChanged?.Invoke();
//    }

//    public bool Contains(TValue item) => Values.Contains(item);

//    public void CopyTo(TValue[] array, int arrayIndex = 0) => Values.CopyTo(array, arrayIndex);

//    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
//    public IEnumerator<TValue> GetEnumerator() => Values.GetEnumerator();

//    public int IndexOf(TValue item) => Values.IndexOf(item);

//    public bool Insert(int index, TValue item)
//    {
//        if (Values.Contains(item))
//            return false;
//        int newId = NewUniqeId(item);
//        Values.Insert(index,item);
//        PlayerPrefs.SetString(Uniqekey + newId.ToString(toStringFormat), JsonConvert.SerializeObject(item));
//        UniqeIds.Insert(index, newId);
//        PlayerPrefs.SetString(Uniqekey + "Ids", JsonConvert.SerializeObject(UniqeIds));
//        OnChanged?.Invoke();
//        return true;
//    }

//    public bool Remove(TValue item)
//    {
//        int index = Values.IndexOf(item);
//        if(index > -1)
//        {
//            RemoveAt(index);
//            Values.RemoveAt(index);
//            OnChanged?.Invoke();
//            return true;
//        }
//        return false;
//    }

//    public void RemoveAt(int index)
//    {
//        Values.RemoveAt(index);
//        PlayerPrefs.DeleteKey(Uniqekey + UniqeIds[index].ToString(toStringFormat));
//        UniqeIds.RemoveAt(index);
//        PlayerPrefs.SetString(Uniqekey + "Ids", JsonConvert.SerializeObject(UniqeIds));
//        OnChanged?.Invoke();
//    }

//    public void Save()
//    {
//        PlayerPrefs.SetString(Uniqekey + "Ids", JsonConvert.SerializeObject(UniqeIds));
//        for (int i = 0; i < Values.Count; i++)
//            PlayerPrefs.SetString(Uniqekey + UniqeIds[i].ToString(toStringFormat), JsonConvert.SerializeObject(Values[i]));
//        PlayerPrefs.Save();
//    }

//    public void ExceptWith(IEnumerable<TValue> other)
//    {
//        throw new NotImplementedException();
//    }
//    public bool SetEquals(IEnumerable<TValue> other)
//    {
//        throw new NotImplementedException();
//    }
//    public void SymmetricExceptWith(IEnumerable<TValue> other)
//    {
//        throw new NotImplementedException();
//    }
//    public void UnionWith(IEnumerable<TValue> other)
//    {
//        throw new NotImplementedException();
//    }
//    public void IntersectWith(IEnumerable<TValue> other)
//    {
//        throw new NotImplementedException();
//    }
//    public bool IsProperSubsetOf(IEnumerable<TValue> other)
//    {
//        throw new NotImplementedException();
//    }
//    public bool IsProperSupersetOf(IEnumerable<TValue> other)
//    {
//        throw new NotImplementedException();
//    }
//    public bool IsSubsetOf(IEnumerable<TValue> other)
//    {
//        throw new NotImplementedException();
//    }
//    public bool IsSupersetOf(IEnumerable<TValue> other)
//    {
//        throw new NotImplementedException();
//    }
//    public bool Overlaps(IEnumerable<TValue> other)
//    {
//        throw new NotImplementedException();
//    }



//    int NewUniqeId(TValue obj)
//    {
//        int hash = obj.GetHashCode();
//        if (!UniqeIds.Contains(hash))
//            return hash;
//        int tryCount = 0;
//        while (tryCount < 1000) //Sonsuz döngüye düşmemesi için
//        {
//            var newValue = UnityEngine.Random.Range(0, 999999);
//            if (!UniqeIds.Contains(newValue))
//                return newValue;
//            tryCount++;
//        }
//        return -1;
//    }


//}

//public static class HashSetPrefsExt
//{
//    public static TObj Find<TObj>(this HashSet_<TObj> l, Predicate<TObj> match)
//    {
//        foreach (var item in l)
//        {
//            if (match(item))
//                return item;
//        }
//        return default(TObj);
//    }
//    public static int FindIndex<TObj>(this HashSet_<TObj> l, Predicate<TObj> match)
//    {
//        int ind = -1;
//        foreach (var item in l)
//        {
//            ind++;
//            if (match(item))
//                return ind;
//        }
//        return -1;
//    }
//    public static TObj[] FindAll<TObj>(this HashSet_<TObj> l, Predicate<TObj> match)
//    {
//        List<TObj> temp = new List<TObj>();
//        foreach (var item in l)
//        {
//            if (match(item))
//                temp.Add( item);
//        }
//        return temp.ToArray();
//    }

//    public static bool Exists<TObj>(this HashSet_<TObj> l,Func<TObj, bool> p)
//    {
//        foreach (var v in l)
//            if (p(v))
//                return true;
//        return false;
//    }

//}