using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Composites;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;
using UnityEngine.UI;

[AddComponentMenu("Input/OnScreenToggle")]
public class OnScreenToggle : OnScreenControl
{
    private Toggle toggle;

    public AxisComposite comp;

    [InputControl(layout = "Axis")]
    [SerializeField]
    private string m_ControlPath;

    protected override string controlPathInternal
    {
        get => m_ControlPath;
        set => m_ControlPath = value;
    }

    private void Awake()
    {
        toggle = GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(OnValueChanged);
    }

    void OnValueChanged(bool v)
    {
        SendValueToControl(v ? 1f : 0f);
    }
}