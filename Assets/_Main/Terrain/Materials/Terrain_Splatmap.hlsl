 int _UseRandMask;
  float _RandMaskValue;

  float rand2 (float2 coords){
         return frac(sin(dot(coords, float2(12.9898,78.233))) * 43758.5453);
  }

  float2 mixrotation(float2 uv_float2){
      if(_UseRandMask)
      {
          float2 samp = uv_float2;
          float r = (round(rand2(ceil(samp)) * _RandMaskValue));
          float m1 = ((r - 1) * (3 - r)) / min(r - 3, -1);
          float m2 = (r * (2 - r)) / max(r, 1);
          float m3 = (r * (r - 2)) / max(r, 1);
          float m4 = ((3 - r) * (r - 1)) / min(r-3, -1);
    
          samp -= 0.5;
          samp = mul(samp, float2x2(m1, m2, m3, m4));
          samp += 0.5;
    
          return samp;
      }
      else{
          return uv_float2;
      }
  }