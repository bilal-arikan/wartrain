using System.Collections.Generic;
using Arikan;
using UnityEngine;

[CreateAssetMenu(fileName = "CustomizeResources", menuName = "GDC_WarTrain/CustomizeResources", order = 0)]
[ResourcePath("Data/CustomizeResources")]
public class CustomizeResources : SingletonScriptableObject<CustomizeResources>
{
    public List<CustomizeItem> initiallyOwnedItems = new List<CustomizeItem>();
    public List<CustomizeItem> customizeItems = new List<CustomizeItem>();
}