using System.Collections.Generic;
using System.Linq;
using Arikan;
using Cinemachine;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public enum CameraMode
{
    None = -1,
    Aim = 0,
    Zoom,
    Mortar,
    Fps,
    Orbit
}

public class CameraSpecificSettings
{
    public Vector2 rotationSpeed = new Vector2(0.5f, 0.5f);
    public float rotationLerp = 0.5f;
    public float VerticalRotMax = 60;
    public float VerticalRotMin = -60;
}

public class CameraController : SingletonBehaviour<CameraController>
{
    public CinemachineBrain brain;
    public CinemachineFreeLook orbitVirtCamera;

    public Dictionary<CameraMode, CinemachineVirtualCameraBase> cameraDict = new Dictionary<CameraMode, CinemachineVirtualCameraBase>();
    public Dictionary<CameraMode, CameraSpecificSettings> cameraSettings = new Dictionary<CameraMode, CameraSpecificSettings>();

    [Space]
    public float cameraSpeedMultiply = 3;
    public float deadZoneX = 0.2f;
    public float orbitCamTurnSpeed = 150f;
    public CameraMode currentMode => cameraDict.FirstOrDefault(cd => cd.Value.gameObject.activeSelf).Key;
    public CinemachineVirtualCameraBase currentCamera => cameraDict.FirstOrDefault(cd => cd.Value.gameObject.activeSelf).Value;

    private float oldLookActivateInput;
    private Quaternion firstLookRotation;
    private bool isPointerOverUI;

    protected override void Awake()
    {
        base.Awake();
        ChangeCameraMode(CameraMode.Aim);
        CinemachineCore.GetInputAxis = GetAxisCustom;
        orbitVirtCamera.m_XAxis.m_MaxSpeed = orbitCamTurnSpeed;
    }
    private void Update()
    {
        var mode = currentMode;
        if (InputNew.LookActivateInput != oldLookActivateInput)
        {
            isPointerOverUI = ScreenUtils.IsPointerOverUIObjectNew();
        }

        if (InputNew.LookActivateInput > 0 && !isPointerOverUI)
        {
            if (mode == CameraMode.Orbit)
            {
                CinemachineCore.GetInputAxis = GetAxisCustom;
            }
            else
            {
                foreach (var camDictKV in cameraDict)
                {
                    if (!camDictKV.Value.gameObject.activeSelf)
                        continue;
                    var sett = cameraSettings[camDictKV.Key];
                    if (oldLookActivateInput <= 0)
                    {
                        firstLookRotation = camDictKV.Value.Follow.rotation;
                    }

                    //Rotate the Follow Target transform based on the input
                    firstLookRotation *= Quaternion.AngleAxis(InputNew.LookInput.x * sett.rotationSpeed.y * cameraSpeedMultiply, Vector3.up);
                    firstLookRotation *= Quaternion.AngleAxis(-InputNew.LookInput.y * sett.rotationSpeed.x * cameraSpeedMultiply, Vector3.right);

                    var targetEularAngle = firstLookRotation.eulerAngles;
                    targetEularAngle.z = 0;

                    //Clamp the Up/Down rotation
                    if (Mathf.Sign(sett.VerticalRotMax) == Mathf.Sign(sett.VerticalRotMin))
                    {
                        if (sett.VerticalRotMax > 0)
                        {
                            targetEularAngle.x = Mathf.Clamp(targetEularAngle.x, sett.VerticalRotMin, sett.VerticalRotMax);
                        }
                        else
                        {
                            // Debug.Log(targetEularAngle.x);
                            // targetEularAngle.x = Mathf.Clamp(targetEularAngle.x, 360 + sett.VerticalRotMin, 360 + sett.VerticalRotMax) - 360;
                        }
                    }
                    else if (targetEularAngle.x > 180 && targetEularAngle.x < 360 + sett.VerticalRotMin)
                    {
                        targetEularAngle.x = 360 + sett.VerticalRotMin;
                    }
                    else if (targetEularAngle.x < 180 && targetEularAngle.x > sett.VerticalRotMax)
                    {
                        targetEularAngle.x = sett.VerticalRotMax;
                    }

                    var targetRotation = Quaternion.Lerp(camDictKV.Value.Follow.rotation, Quaternion.Euler(targetEularAngle), Time.deltaTime * sett.rotationLerp);
                    // Rotate even disabled cameras
                    foreach (var camDictKV2 in cameraDict)
                    {
                        camDictKV2.Value.Follow.rotation = targetRotation;
                    }

                }
            }
        }
        oldLookActivateInput = InputNew.LookActivateInput;
    }
    private void LateUpdate()
    {

    }

    public void Shake()
    {
        // Camera.main.DOShakePosition(0.2f, 3, 2);
    }

    public float GetAxisCustom(string axisName)
    {
        if (InputNew.LookActivateInput > 0 && !isPointerOverUI)
        {
            var LookCamera = InputNew.LookInput.normalized; // reads theavailable camera values and uses them.

            if (axisName == "Mouse X")
            {
                if (LookCamera.x > deadZoneX || LookCamera.x < -deadZoneX) // To stabilise Cam and prevent it from rotating when LookCamera.x value is between deadZoneX and - deadZoneX
                {
                    return LookCamera.x;
                }
            }
            else if (axisName == "Mouse Y")
            {
                return LookCamera.y;
            }
        }
        return 0;
    }
    [Button]
    public void ChangeCameraMode(CameraMode mode)
    {
        foreach (var camDictKV in cameraDict)
        {
            camDictKV.Value.gameObject.SetActive(camDictKV.Key == mode);
        }
        GameView.Instance.UpdateUI();
    }
    public void SetTarget(Transform target)
    {
        foreach (var camDictKV in cameraDict)
        {
            camDictKV.Value.LookAt = target;
            camDictKV.Value.Follow = target;
        }
    }
}