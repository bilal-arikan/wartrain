using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class LostView : UIView<LostView>
{
    public Button startButton;
    public Button watchAdButton;
    public Button toMenuButton;
    [Header("Rewards")]
    public Text moneyText;
    public Text goldText;
    public Text xpText;

    public override string Group => "main";

    // Start is called before the first frame update
    void Start()
    {
        startButton.onClick.AddListener(GameController.Instance.RestartGame);
        watchAdButton.onClick.AddListener(WatchAd);
        toMenuButton.onClick.AddListener(GameController.Instance.ToMenu);
        toMenuButton.onClick.AddListener(Close);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        // DataController.Money.OnBeforeChanged += (v1, v2) => moneyText.Counter(v1, v2, 0.5f, 0.1f, format: "${0}").RunAsPromise(this);
    }

    void WatchAd()
    {
        AdsSystem.Instance.ShowReward((s) =>
        {
            if (s)
            {
            }
        });
    }
    public void Parse()
    {
        moneyText.text = ": ";
        goldText.text = ": ";
        xpText.text = ": ";
    }
}
