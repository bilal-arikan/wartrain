using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Arikan;
using DG.Tweening;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

public class GameView : UIView<GameView>
{
    public Button pauseButton;
    public Button iapButton;
    [Space]
    public Slider accelerateSlider;
    public Slider speedSlider;
    public Text speedText;
    public Toggle breakToggle;
    public Toggle zoomToggle;
    public Toggle autoFireToggle;
    public VagonToggleItem[] weaponToggles;
    [Header("Level Info")]
    public Slider levelProgressSlider;
    public Text enemiesText;
    public Dictionary<EnemyType, int> enemyCounts = new Dictionary<EnemyType, int>();
    public RectTransform indicatorsRoot;
    [Header("Effects")]
    public float targetHitDuration = 0.2f;
    public Image targetHitImage;
    private Tween targetHitTween;
    public Image aimLockedImage;
    [Header("Damage Feedback")]
    public float damagedDuration = 0.2f;
    public Image damagedImage;
    private Tween damagedTween;
    public Image vagonHealth;
    public Text vagonHealthText;

    public override string Group => "main";

    private CameraMode lastCameraMode = CameraMode.Orbit;

    void Start()
    {
        pauseButton.onClick.AddListener(PausedView.Instance.Open);
        iapButton.onClick.AddListener(IAPView.Instance.Open);
        zoomToggle.onValueChanged.AddListener(v =>
        {
            if (v)
                lastCameraMode = CameraController.Instance.currentMode;
            if (v)
                CameraController.Instance.ChangeCameraMode(CameraMode.Zoom);
            else
                CameraController.Instance.ChangeCameraMode(lastCameraMode);
        });
        autoFireToggle.onValueChanged.AddListener(v =>
        {
            DataController.AutoFireOn.Value = v;
        });
        for (int i = 0; i < weaponToggles.Length; i++)
        {
            int index = i;
            weaponToggles[i].vagonName.text = (i + 1).ToString();
            weaponToggles[i].toggle.onValueChanged.AddListener(v =>
            {
                if (v)
                {
                    PlayerController.Instance.CameraFollow(index);
                }
            });
        }
        UpdateUI();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        accelerateSlider.value = 0;
        speedSlider.value = 0;
        levelProgressSlider.value = 0;
        speedText.text = "00";
        breakToggle.isOn = false;
        TopPanel.Instance.Close();
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        TopPanel.Instance.Open();
    }

    private void Update()
    {
        var train = PlayerController.Instance.currentTrain;
        if (train)
        {
            levelProgressSlider.value = train.walker.NormalizedT;
            speedSlider.value = train.currentSpeed / train.maxSpeed;
            speedText.text = train.currentSpeed.ToString("00");
            aimLockedImage.transform.localScale = Vector3.one * Mathf.Clamp((1 + train.weaponsActivatedVagon.autoAimDifferenceAmount), 1, 3);
        }
    }

    public override void UpdateUI()
    {
        if (!Application.isPlaying)
            return;
        var train = PlayerController.Instance?.currentTrain;
        if (train)
        {
            speedSlider.value = train.currentSpeed / train.maxSpeed;
            speedText.text = train.currentSpeed.ToString("00");
            zoomToggle.SetIsOnWithoutNotify(CameraController.Instance.currentMode == CameraMode.Zoom);
            breakToggle.SetIsOnWithoutNotify(train.isBreaking);
            autoFireToggle.SetIsOnWithoutNotify(DataController.AutoFireOn);

            for (int i = 0; i < weaponToggles.Length; i++)
            {
                var vgn = train.vagonsWithHead.ElementAt(i);
                weaponToggles[i].toggle.SetIsOnWithoutNotify(train.vagonsWithHead.IndexOf(train.weaponsActivatedVagon) == i);
            }
        }

        var stage = LevelController.Instance.GetCurrentStage();
        if (stage != null)
        {
            var targets = stage.spawnedObjectsByStage;
            enemiesText.text = "Enemies: " + targets.Where(t => !t.IsDeath).Count() + "/" + targets.Count;
        }
    }

    public ArrowIndicator AddIndicator(Transform target, ArrowIndicator indPrefab, Vector3 offset)
    {
        var ind = Instantiate(indPrefab, indicatorsRoot);
        ind.offsetWorldPosition = offset;
        ind.uiCamera = Camera.main;
        ind.target = target;
        return ind;
    }
    public void RemoveIndicator(Transform target) => RemoveIndicator(FindObjectsOfType<ArrowIndicator>().FirstOrDefault(i => i.target == target));
    public void RemoveIndicator(ArrowIndicator ind)
    {
        if (ind != null)
        {
            Destroy(ind.gameObject);
        }
    }

    public void SetVagonHealth(int health, int maxHealth)
    {
        vagonHealth.fillAmount = (float)health / maxHealth;
        vagonHealthText.text = health.ToString();
    }

    public void ShowTargetHit()
    {
        targetHitImage.color = targetHitImage.color.WithAlpha(1);
        targetHitTween?.Kill();
        targetHitTween = targetHitImage.DOFade(0, targetHitDuration);
    }
    public void ShowDamaged(float value = 1f)
    {
        damagedImage.color = damagedImage.color.WithAlpha(value);
        damagedTween?.Kill();
        damagedTween = damagedImage.DOFade(0, damagedDuration);
    }
    public void OpenConsole() => SRDebug.Instance.ShowDebugPanel(false);
}
