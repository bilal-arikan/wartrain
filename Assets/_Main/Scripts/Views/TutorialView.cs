using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class TutorialView : UIView<TutorialView>
{
    [SerializeField] Button closeButton;

    public GameObject[] tutorialSteps;

    public override string Group => "";

    void Start()
    {
        closeButton.onClick.AddListener(Close);
    }

    public void Open(int index)
    {
        for (int i = 0; i < tutorialSteps.Length; i++)
        {
            tutorialSteps[i].SetActive(i == index);
        }
        base.Open();
    }
}