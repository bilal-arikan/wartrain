using UnityEngine;
using UnityEngine.UI;

public class LevelButtonItem : MonoBehaviour
{
    public Button button;
    public Text levelText;
    [Space]
    public GameObject completed;
    public GameObject locked;
    [Space]
    public GameObject medalBronze;
    public GameObject medalSilver;
    public GameObject medalGold;

}