using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BezierSolution;
using EasyRoads3Dv3;
using UnityEngine;

public class RailwayMeshGenerator : MonoBehaviour
{
    public ERRoadNetwork roadNetwork;
    public ERRoad road;
    public BezierSpline pathSpline;
    public int roadPointCount = 50;

    public Vector3[] markers = new Vector3[]{
        new Vector3(200, 5, 200),
        new Vector3(250, 5, 200),
        new Vector3(250, 5, 250),
        new Vector3(300, 5, 250),
    };

    // Start is called before the first frame update
    void OnEnable()
    {
        if (road != null)
        {
            DestroyImmediate(road.gameObject);
        }
        roadNetwork = new ERRoadNetwork();

        // create a new road type
        ERRoadType roadType = new ERRoadType();
        roadType.roadWidth = 6;
        roadType.roadMaterial = Resources.Load("Materials/roads/road material") as Material;
        // optional
        roadType.layer = 1;
        roadType.tag = "Untagged";
        roadType.hasMeshCollider = false; // default is true

        //		roadType = roadNetwork.GetRoadTypeByName("Train Rail");
        //		Debug.Log(roadType.roadMaterial);

        markers = Enumerable
            .Range(0, roadPointCount)
            .Select(i => pathSpline.GetPoint((float)i / (roadPointCount - 1)).AddY(0.0f))
            .ToArray();

        // create a new road
        road = roadNetwork.CreateRoad("RailRoad", roadType, markers);
        // road.AddMarker(new Vector3(300, 5, 300));
        // road.InsertMarker(new Vector3(275, 5, 235), 3);
        // road.DeleteMarker(2);

        // Set the road width : ERRoad.SetWidth(float width);
        //	road.SetWidth(10);

        // Set the road material : ERRoad.SetMaterial(Material path);
        //	Material mat = Resources.Load("Materials/roads/single lane") as Material;
        //	road.SetMaterial(mat);

        // add / remove a meshCollider component
        //   road.SetMeshCollider(bool value):void;

        // set the position of a marker
        //   road.SetMarkerPosition(int index, Vector3 position):void;
        //   road.SetMarkerPositions(Vector3[] position):void;
        //   road.SetMarkerPositions(Vector3[] position, int index):void;

        // get the position of a marker
        //   road.GetMarkerPosition(int index):Vector3;

        // get the position of a marker
        //   road.GetMarkerPositions():Vector3[];

        // set marker control type
        //  road.SetMarkerControlType(int marker, ERMarkerControlType type) : bool; // Spline, StraightXZ, StraightXZY, Circular

        // find a road
        //  public static function ERRoadNetwork.GetRoadByName(string name) : ERRoad;

        // get all roads
        //  public static function ERRoadNetwork.GetRoads() : ERRoad[];  

        // snap vertices to the terrain (no terrain deformation)
        //		road.SnapToTerrain(true);

        // Build Road Network
        roadNetwork.BuildRoadNetwork();

        // remove script components
        //		roadNetwork.Finalize();

        // Restore Road Network 
        //	roadNetwork.RestoreRoadNetwork();

        // Show / Hide the white surfaces surrounding roads
        //     public function roadNetwork.HideWhiteSurfaces(bool value) : void;

        //   road.GetConnectionAtStart(): GameObject;
        //   road.GetConnectionAtStart(out int connection): GameObject; // connections: 0 = bottom, 1= tip, 2 = left, 3 = right (the same for T crossings)

        //   road.GetConnectionAtEnd(): GameObject;
        //   road.GetConnectionAtEnd(out int connection): GameObject; // connections: 0 = bottom, 1= tip, 2 = left, 3 = right (the same for T crossings)

        // Snap the road vertices to the terrain following the terrain shape (no terrain deformation)
        //   road.SnapToTerrain(bool value): void;
        //   road.SnapToTerrain(bool value, float yOffset): void;

        // get the road length
        //   road.GetLength() : float;
    }
    void OnDisable()
    {
        // Restore road networks that are in Build Mode
        // This is very important otherwise the shape of roads will still be visible inside the terrain!

        if (roadNetwork != null)
        {
            if (roadNetwork.isInBuildMode)
            {
                roadNetwork.RestoreRoadNetwork();
                Debug.Log("Restore Road Network");
            }
        }
    }
    float distance = 0;
    int currentElement;
    // void Update()
    // {
    //     if (roadNetwork != null)
    //     {
    //         float deltaT = Time.deltaTime;
    //         float rSpeed = (deltaT * 5); // speed

    //         distance += rSpeed;

    //         // pass the current distance to get the position on the road
    //         //			Debug.Log(road);
    //         Vector3 v = road.GetPosition(distance, ref currentElement);
    //         v.y += 1;

    //         // go.transform.position = v;
    //         // go.transform.forward = road.GetLookatSmooth(distance, currentElement); ;
    //     }

    //     // spline point info center of the road
    //     //      public function ERRoad.GetSplinePointsCenter() : Vector3[];

    //     // spline point info center of the road
    //     //      public function ERRoad.GetSplinePointsRightSide() : Vector3[];

    //     // spline point info center of the road
    //     //      public function ERRoad.GetSplinePointsLeftSide() : Vector3[];

    //     // Get the selected road in the Unity Editor
    //     //  public static function EREditor.GetSelectedRoad() : ERRoad;   
    // }
    private void OnDrawGizmos()
    {
        GizmosUtils.DrawPath(markers);
    }
}
