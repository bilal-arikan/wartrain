using Sirenix.OdinInspector;
using UnityEngine;

public abstract class CustomizeItem : ScriptableObject
{
    [ShowInInspector] public string id => name;
    [ShowInInspector] public abstract string ctgry { get; }
    public int price;
    public string definition;
    public Sprite icon;
    public abstract UnityEngine.Object asset { get; }
}