using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public enum Team
{
    Player,
    Rebel,
    Civilian
}

public class TargetBehaviour : MonoBehaviour
{
    public Team team;
    public int maxHealth = 0;
    public int health = 10;
    [ShowInInspector] public bool IsDeath => health == 0;
    public Collider fireArea;
    [Header("Indicators")]
    public Vector3 indicatorOffset;
    public IndicatorFillAmount indicatorPrefab;
    protected IndicatorFillAmount healthIndicator;

    public UnityEvent onDeath;
    public UnityEvent onDamaged;

    private void Awake()
    {
        if (maxHealth < health)
            maxHealth = health;
        gameObject.tag = "Shootable";
    }
    private void Start()
    {
    }

    public void OnHitBy(BulletBehaviour bullet)
    {
        if (IsDeath)
            return;
        health = Mathf.Clamp(health - bullet.damage, 0, maxHealth);
        if (healthIndicator)
        {
            healthIndicator.SetValue(health / (float)maxHealth);
        }

        onDamaged.Invoke();
        if (health == 0)
        {
            Death();
        }
    }

    [Button]
    void Death()
    {
        Debug.Log("Death " + this, this);
        health = 0;
        if (fireArea)
        {
            fireArea?.gameObject.SetActive(false);
        }

        onDeath.Invoke();
        GameView.Instance.UpdateUI();
    }

    public void SetIndicatorVisible(bool visible)
    {
        if (visible)
        {
            if (healthIndicator == null)
            {
                healthIndicator = GameView.Instance.AddIndicator(transform, indicatorPrefab.Indicator, indicatorOffset).GetComponent<IndicatorFillAmount>();
                healthIndicator.SetValue(health / (float)maxHealth);
            }
        }
        else
        {
            if (healthIndicator != null)
            {
                GameView.Instance.RemoveIndicator(healthIndicator.Indicator);
                healthIndicator = null;
            }
        }
    }
}