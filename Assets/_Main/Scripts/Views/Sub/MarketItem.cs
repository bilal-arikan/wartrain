using UnityEngine;
using UnityEngine.UI;

public class MarketItem : MonoBehaviour
{
    public Button button;
    public Text info;
    public Text price;
    public Image thumbnail;
    public GameObject owned;
    public GameObject selected;


    public CustomizeItem Item { get; private set; }

    private void Awake()
    {
        button.onClick.AddListener(OnClick);
    }
    public MarketItem Init(CustomizeItem item)
    {
        this.Item = item;
        info.text = item.definition;
        thumbnail.sprite = item.icon;
        price.text = "";
        return this;
    }
    private void OnClick()
    {
        PurchaseView.Instance.ParseItem(Item);
        PurchaseView.Instance.Open();
    }
    public void UpdateUI()
    {
        if (Item == null) return;
        owned.SetActive(CustomizeController.Instance.IsItemOwned(Item.id));
        selected.SetActive(CustomizeController.Instance.IsItemApplied(Item.id));
    }
}