using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class StageActivatorTrigger : MonoBehaviour
{
    public LevelStage stage;

    private void Start()
    {
        stage = GetComponentInParent<LevelStage>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody != null && other.attachedRigidbody.gameObject.TryGetComponent<VagonBehaviour>(out var vagon))
        {
            stage.AlarmStage();
        }
        gameObject.SetActive(true);
    }
}