using UnityEngine;
using UnityEngine.UI;

public class VagonToggleItem : MonoBehaviour
{
    public Toggle toggle;
    public Text vagonName;
    public Image vagonHealth;
    public Text vagonHealthText;
}