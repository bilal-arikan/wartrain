using System.Collections.Specialized;
using UnityEngine;

public class AutoRotate : MonoBehaviour
{
    public Vector3 angularSpeed;

    private void Update()
    {
        transform.localEulerAngles += angularSpeed * Time.deltaTime;
    }
}