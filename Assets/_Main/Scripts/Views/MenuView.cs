using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class MenuView : UIView<MenuView>
{
    public Button startButton;
    public Button marketButton;
    public Button inventButton;
    public Button settingsButton;


    public override string Group => "main";

    // Start is called before the first frame update
    void Start()
    {
        startButton.onClick.AddListener(LevelsView.Instance.Open);
        marketButton.onClick.AddListener(MarketView.Instance.Open);
        // inventButton.onClick.AddListener(InventoryView.Instance.Open);
        settingsButton.onClick.AddListener(SettingsView.Instance.Open);
    }
}
