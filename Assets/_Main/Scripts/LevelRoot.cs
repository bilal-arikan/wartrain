using System;
using System.Collections.Generic;
using System.Linq;
using BezierSolution;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class LevelRoot : SerializedMonoBehaviour
{
    public BezierSpline splinePath;
    [Range(0, 1f)]
    public float[] gateNormalizedTs = new float[0];

    private void Awake()
    {
        splinePath = splinePath ?? GetComponentInChildren<BezierSpline>();
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < gateNormalizedTs.Length; i++)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawCube(splinePath.GetPoint(gateNormalizedTs[i]), Vector3.one * 0.5f);
        }
    }
}