using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class IAPView : UIView<IAPView>
{
    public Button closeButton;

    public override string Group => "";

    // Start is called before the first frame update
    void Start()
    {
        closeButton.onClick.AddListener(Close);
    }
}
