using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class PurchaseView : UIView<PurchaseView>
{
    [SerializeField] Button closeButton;
    [SerializeField] Button purchaseButton;
    [SerializeField] Button ownedButton;
    [SerializeField] Button appliedButton;
    [SerializeField] Button watchAdButton;
    [SerializeField] public Text priceText;
    [SerializeField] public Text itemDefinition;
    [SerializeField] public Image itemThumbnail;

    private CustomizeItem current;

    public override string Group => "";

    void Start()
    {
        closeButton.onClick.AddListener(Close);
        purchaseButton.onClick.AddListener(TryPurchase);
        ownedButton.onClick.AddListener(TryUse);
        watchAdButton.onClick.AddListener(WatchAd);
    }

    public void ParseItem(CustomizeItem item)
    {
        current = item;
        var owned = CustomizeController.Instance.IsItemOwned(item.id);
        var applied = CustomizeController.Instance.IsItemApplied(item.id);

        priceText.text = ": " + (!owned ? item.price.ToString() : "OWNED");
        itemDefinition.text = item.definition;
        itemThumbnail.sprite = item.icon;

        purchaseButton.gameObject.SetActive(!owned);
        watchAdButton.gameObject.SetActive(!owned);
        ownedButton.gameObject.SetActive(owned && !applied);
        appliedButton.gameObject.SetActive(owned && applied);
    }

    public void TryPurchase()
    {
        CustomizeController.Instance.BuyItem(current.id, (r) =>
        {
            MarketView.Instance.UpdateUI();
            Close();
        });

    }
    public void TryUse()
    {
        CustomizeController.Instance.ApplyItem(current.id, (r) =>
        {
            MarketView.Instance.UpdateUI();
            Close();
        });
    }
    public void WatchAd()
    {

    }
}