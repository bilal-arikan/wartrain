using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class IndicatorFillAmount : MonoBehaviour
{
    public ArrowIndicator Indicator;
    public Image Image;

    public void SetValue(float v)
    {
        Image.fillAmount = v;
    }
}
