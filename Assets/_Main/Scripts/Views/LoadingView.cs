using System.Collections;
using System.Collections.Generic;
using Arikan;
using UnityEngine;
using UnityEngine.UI;

public class LoadingView : UIView<LoadingView>
{
    [SerializeField] Button closeButton;

    public override string Group => "";

    void Start()
    {
        closeButton.onClick.AddListener(Close);
    }
}