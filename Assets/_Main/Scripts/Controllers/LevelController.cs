using System.Collections.Generic;
using System.Linq;
using Arikan;
using BezierSolution;
using Sirenix.OdinInspector;
using UnityEngine;

public class LevelController : SingletonBehaviour<LevelController>
{
    [ShowInInspector, ReadOnly] public int CurrentLevelIndex { get; private set; }
    [ShowInInspector] public LevelRoot currentLevelRoot;
    [ShowInInspector] public BezierSpline currentTrainPath;

    [ShowInInspector] [ReadOnly] public float noramlizedStartPoint { get; private set; }
    [ShowInInspector, ReadOnly] private List<LevelStage> stageRoots = new List<LevelStage>();
    private int currentStageIndex = -1;
    public GateWayBehaviour currentGateWay { get; set; }
    public bool isBreakNecessary => !(currentGateWay == null || currentGateWay.IsOpened);

    [Button]
    public LevelRoot LoadLevel(int i)
    {
        currentLevelRoot = Instantiate(GameResources.Instance.levels[i], GameResources.Instance.levels[i].transform.position, Quaternion.identity);
        CurrentLevelIndex = i;
        currentStageIndex = 0;
        currentTrainPath = currentLevelRoot.splinePath;

        var noramlizedFinishPoint = GameResources.Instance.levelFinishNormalizedT;// (length - 60f) / length;
        var finishGate = Instantiate(
            GameResources.Instance.finishGatePrefab,
            currentTrainPath.GetPoint(noramlizedFinishPoint),
            Quaternion.LookRotation(currentTrainPath.GetTangent(noramlizedFinishPoint), Vector3.up));
        finishGate.isFinishGate = true;
        finishGate.openType = GateOpenType.Timer;
        finishGate.openTimerDuration = 1f;

        MountainsBehaviour.Instance.SpawnMountains(GameResources.Instance.mountainPrefabs.GetRandom());
        MountainsBehaviour.Instance.transform.position = currentLevelRoot.transform.position.AddX(500).AddZ(500);

        stageRoots.Clear();
        stageRoots = currentLevelRoot
            .GetComponentsInChildren<LevelStage>()
            .OrderBy(x => x.transform.GetSiblingIndex())
            .ToList();
        ActivateStage(0);

        return currentLevelRoot;
    }
    [Button]
    public void RemoveLevel()
    {
        MountainsBehaviour.Instance.ClearMountains();
        if (currentLevelRoot != null)
        {
            Destroy(currentLevelRoot.gameObject);
            currentLevelRoot = null;
        }
    }
    [Button]
    public void ActivateStage(int id)
    {
        if (id >= stageRoots.Count)
        {
            // GameController.Instance.FinishLevel();
            return;
        }

        for (int i = 0; i < stageRoots.Count; i++)
        {
            stageRoots[i].gameObject.SetActive(id == i || id == i - 1 || id == i + 1);
        }

        currentStageIndex = id;
        GameView.Instance.UpdateUI();
        Debug.Log("SpawnStage " + id, stageRoots[id]);
    }
    public void NextStage() => ActivateStage(currentStageIndex + 1);

    [Button]
    public void HideStage(int id)
    {
        var stg = stageRoots[id];
        Debug.Log("HideStage " + id, stg);
        stg.gameObject.SetActive(false);
    }
    public LevelStage GetCurrentStage()
    {
        if (currentStageIndex >= 0 && currentStageIndex < stageRoots.Count)
        {
            return stageRoots[currentStageIndex];
        }
        else
        {
            return null;
        }
    }
    public EnemyBehaviour[] GetStageEnemies() => GetCurrentStage().spawnedObjectsByStage.Select(o => o.GetComponent<EnemyBehaviour>()).Where(o => o != null).ToArray();

    public TrainBehaviour SpawnTrain(params VagonBehaviour[] fullTrainPrefabs)
    {
        TrainBehaviour currentTrain = null;
        // var length = currentTrainPath.GetLengthApproximately(0f, 1f);
        noramlizedStartPoint = GameResources.Instance.trainSpawnNormalizedT;// 60f / length;
        for (int i = 0; i < fullTrainPrefabs.Length; i++)
        {
            if (i == 0)
            {
                currentTrain = Instantiate(fullTrainPrefabs[i], currentTrainPath.GetPoint(noramlizedStartPoint), Quaternion.identity) as TrainBehaviour;
                currentTrain.walker.spline = currentTrainPath;
                currentTrain.walker.NormalizedT = noramlizedStartPoint;
                currentTrain.locomotion.walker = currentTrain.walker;
            }
            else
            {
                var vagon = Instantiate(fullTrainPrefabs[i], fullTrainPrefabs[i].transform.position, Quaternion.identity);
                currentTrain.LinkVagon(vagon, i);
            }
        }
        return currentTrain;
    }
    public void RemoveTrain(TrainBehaviour train)
    {
        if (train != null)
        {
            for (int i = 0; i < train.vagons.Count; i++)
            {
                Destroy(train.vagons[i].gameObject);
            }
            Destroy(train.gameObject);
        }
    }
}