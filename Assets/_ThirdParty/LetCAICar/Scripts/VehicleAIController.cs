﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class VehicleAIController : MonoBehaviour
{
    public List<Transform> waypoints = new List<Transform>();
    private Cart cart;
    public float steeringSensitivity = 0.01f;
    public float breakingSensitivity = 1.0f;
    public float accelerationSensitivity = 0.3f;

    public GameObject trackerPrefab;
    NavMeshAgent agent;

    int currentTrackerWP;
    float lookAhead = 10;

    float lastTimeMoving = 0;

    // Start is called before the first frame update
    void OnEnable()
    {
        cart = GetComponent<Cart>();
        GameObject tracker = Instantiate(trackerPrefab, cart.transform.position, cart.transform.rotation) as GameObject;
        agent = tracker.GetComponent<NavMeshAgent>();
        currentTrackerWP = Random.Range(0, waypoints.Count);
    }
    private void OnDisable()
    {
        cart.AccelerateCart(0, 0, 1);
        if (agent != null)
        {
            Destroy(agent.gameObject);
        }
    }

    void ProgressTracker()
    {
        if (Vector3.Distance(agent.transform.position, cart.transform.position) > lookAhead)
        {
            agent.isStopped = true;
            return;
        }
        else
        {
            agent.isStopped = false;
        }

        agent.SetDestination(waypoints[currentTrackerWP].position);


        if (Vector3.Distance(agent.transform.position, waypoints[currentTrackerWP].position) < 4)
        {
            currentTrackerWP++;
            if (currentTrackerWP >= waypoints.Count)
                currentTrackerWP = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        ProgressTracker();

        Vector3 localTarget;
        float targetAngle;

        if (cart._rigidbody.velocity.magnitude > 1)
        {
            lastTimeMoving = Time.time;
        }

        if (Time.time > lastTimeMoving + 8)
        {
            cart.transform.position = waypoints[currentTrackerWP].transform.position + Vector3.up * 1;
            agent.transform.position = cart.transform.position;
            lastTimeMoving = Time.time;
        }

        localTarget = cart.transform.InverseTransformPoint(agent.transform.position);
        targetAngle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;


        float steer = Mathf.Clamp(targetAngle * steeringSensitivity, -1, 1) * Mathf.Sign(cart.current_speed);
        float speedFactor = cart.current_speed / 30;
        float corner = Mathf.Clamp(Mathf.Abs(targetAngle), 0, 90);
        float cornerFactor = corner / 90f;

        float brake = 0;
        //if (corner > 10 && speedFactor > 0.1f)
        //    brake = Mathf.Lerp(0, 1 + speedFactor * breakingSensitivity, cornerFactor);

        float accel = 1f;
        if (corner > 20 && speedFactor > 0.1f && speedFactor > 0.2f)
            accel = Mathf.Lerp(0, 1 * accelerationSensitivity, 1 - cornerFactor);

        cart.AccelerateCart(accel, steer, brake);
    }


    private void OnDrawGizmosSelected()
    {
        if (waypoints.Count > 1)
        {
            Vector3 prev = waypoints[0].position;
            for (int i = 1; i < waypoints.Count; i++)
            {
                Vector3 next = waypoints[i].position;
                Gizmos.DrawLine(prev, next);
                prev = next;
            }
            Gizmos.DrawLine(prev, waypoints[0].position);
        }
    }
}
