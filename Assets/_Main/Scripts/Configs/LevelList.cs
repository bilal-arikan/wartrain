using System;
using System.Collections.Generic;
using Arikan;
using Sirenix.OdinInspector;
using UnityEngine;

[Serializable]
public class LevelInfo
{
    public int rewardMoney;
}
[ResourcePath("Data/LevelList")]
[CreateAssetMenu(fileName = "LevelList", menuName = "GDC_WarTrain/LevelList", order = 0)]
public class LevelList : SingletonScriptableObject<LevelList>
{
    [SerializeField] private List<LevelInfo> levels;

    public LevelInfo GetLevelInfo(int index)
    {
        if (index < 0)
        {
            return null;
        }
        if (index >= levels.Count)
        {
            return null;
        }
        return levels[index];
    }

    [Button]
    void ParseFromScene()
    {

    }
}